var searchData=
[
  ['iauditable',['IAuditable',['../interface_app_1_1_helpers_1_1_i_auditable.html',1,'App::Helpers']]],
  ['input',['input',['../class_app_1_1_helpers_1_1_form.html#a19bdf9c278f3bf4f1c5355edcd20bbba',1,'App::Helpers::Form']]],
  ['isactive',['isActive',['../class_app_1_1_models_1_1_project.html#aa56a17bc66c1081fad1cca3a488224c3',1,'App\Models\Project\isActive()'],['../class_app_1_1_models_1_1_user.html#aa56a17bc66c1081fad1cca3a488224c3',1,'App\Models\User\isActive()']]],
  ['isadmin',['isAdmin',['../class_app_1_1_models_1_1_user.html#aabf23b66cd362adaa508de5bfb22706a',1,'App::Models::User']]],
  ['isprivate',['isPrivate',['../class_app_1_1_models_1_1_task.html#a55426c7f8596f9fc117210bf5311d4b9',1,'App::Models::Task']]],
  ['ispublic',['isPublic',['../class_app_1_1_models_1_1_project.html#ac0eb21abcd8889b02b75c545a480fb48',1,'App::Models::Project']]]
];
