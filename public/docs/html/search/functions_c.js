var searchData=
[
  ['redirect',['redirect',['../class_app_1_1_http_1_1_controllers_1_1_controller.html#acea4248a073af4582d5069babd4656b2',1,'App::Http::Controllers::Controller']]],
  ['redirectto',['redirectTo',['../class_app_1_1_http_1_1_middleware_1_1_authenticate.html#afb73afa067aa92166a43b0154f5e279a',1,'App::Http::Middleware::Authenticate']]],
  ['register',['register',['../class_app_1_1_providers_1_1_app_service_provider.html#acc294a6cc8e69743746820e3d15e3f78',1,'App::Providers::AppServiceProvider']]],
  ['render',['render',['../class_app_1_1_exceptions_1_1_handler.html#af86680a7b1f9cf18e5d15e29e6a32dd8',1,'App\Exceptions\Handler\render()'],['../class_app_1_1_helpers_1_1_pager.html#afde88292c44dc59faf017738dae6dffb',1,'App\Helpers\Pager\render()']]],
  ['rendergrid',['renderGrid',['../class_app_1_1_helpers_1_1_grid.html#afea2bb4475fe2c836cf6b1eb6a0cb560',1,'App::Helpers::Grid']]],
  ['report',['report',['../class_app_1_1_exceptions_1_1_handler.html#a154e014b0d8b15cc04a646faded4a69f',1,'App::Exceptions::Handler']]],
  ['row',['row',['../class_app_1_1_helpers_1_1_form.html#a8bbafd170a5e9710e4b8e40d20fb9a9b',1,'App::Helpers::Form']]],
  ['rowsmap',['rowsMap',['../class_app_1_1_helpers_1_1_grid.html#a7293b44d5be4a338051ebf91017eec35',1,'App::Helpers::Grid']]],
  ['rules',['rules',['../class_app_1_1_http_1_1_requests_1_1_holiday_requests_request.html#a17dba92d96b9dd48c62f3ede3eef94d4',1,'App\Http\Requests\HolidayRequestsRequest\rules()'],['../class_app_1_1_http_1_1_requests_1_1_projects_request.html#a17dba92d96b9dd48c62f3ede3eef94d4',1,'App\Http\Requests\ProjectsRequest\rules()'],['../class_app_1_1_http_1_1_requests_1_1_tasks_request.html#a17dba92d96b9dd48c62f3ede3eef94d4',1,'App\Http\Requests\TasksRequest\rules()'],['../class_app_1_1_http_1_1_requests_1_1_workers_request.html#ac5387ea5be680bcde525dae1ff2e2fce',1,'App\Http\Requests\WorkersRequest\rules()']]]
];
