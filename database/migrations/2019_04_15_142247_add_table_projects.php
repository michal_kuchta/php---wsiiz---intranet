<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTableProjects extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('projects', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
            $table->bigInteger('created_by')->nullable();
            $table->bigInteger('updated_by')->nullable();
            $table->string('name');
            $table->text('description');
            $table->boolean('is_public')->default(0);
            $table->boolean('is_active')->default(0);
        });

        Schema::create('project_managers', function (Blueprint $table) {
            $table->bigInteger('user_id');
            $table->bigInteger('project_id');
        });

        Schema::create('project_workers', function (Blueprint $table) {
            $table->bigInteger('user_id');
            $table->bigInteger('project_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('projects');
        Schema::dropIfExists('project_managers');
        Schema::dropIfExists('project_workers');
    }
}
