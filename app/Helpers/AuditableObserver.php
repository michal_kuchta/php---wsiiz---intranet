<?php


namespace App\Helpers;


use App\Models\Project;
use App\Models\ProjectManager;
use App\Models\ProjectWorker;
use Carbon\Carbon;

/**
 * Klasa odpowiada za automatyczną aktualizację dat edycji i tworzenia modelu jak również ustawienia informacji kto utworzył lub edytował model.
 * @package App\Helpers
 *
 */
class AuditableObserver
{
    /**
     * Metoda służy do wygenerowania obecnej daty oraz godziny
     * @return Carbon|\Carbon\CarbonInterface
     */
    private function getDate()
    {
        return Carbon::now(config('app.timezone_display'));
    }

    /**
     * Metoda zwraca identyfikator użytkownika, jeżeli użytkownik jest zalogowany
     * @return int
     */
    private function getUserId()
    {
        return empty(\Auth::id()) ? 0 : \Auth::id();
    }


    /**
     * Metoda ustawia datę edycji oraz tworzenia modelu, jak również id użytkonika, podczast tworzenia modelu.
     * @param IAuditable $model
     */
    public function creating(IAuditable $model)
    {
        $date = $this->getDate();

        $model->created_at = $model->created_at !== null ? $model->created_at : $date;
        $model->updated_at = $date;

        if(isset($model->created_by))
            $model->created_by = $model->created_by !== null ? $model->created_by : $this->getUserId();

        if(isset($model->updated_by))
            $model->updated_by = $this->getUserId();
    }

    /**
     * Metoda zmienia datę oraz id użytkownika, który ostatnio modyfikował model.
     * @param IAuditable $model
     */
    public function updating(IAuditable $model)
    {
        $model->updated_at = $this->getDate();

        if(isset($model->updated_by))
            $model->updated_by = $this->getUserId();

    }

}
