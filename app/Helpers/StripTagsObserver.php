<?php


namespace App\Helpers;


/**
 * Klasa odpowiada za automatyczne usuwanie tagów html z podanych pól formularza.
 * @package App\Helpers
 *
 */
class StripTagsObserver
{
    private $fields = [];

    public function __construct(array $fields = [])
    {
        $this->fields = $fields;
    }

    /**
     * Metoda usunie takig html, podczas tworzenia modelu.
     * @param $model
     */
    public function creating($model)
    {
        foreach ($this->fields as $field)
        {
            $model->{$field} = strip_tags($model->{$field});
        }
    }

    /**
     * Metoda usunie takig html, podczas modyfikacji modelu.
     * @param $model
     */
    public function updating($model)
    {
        foreach ($this->fields as $field)
        {
            $model->{$field} = strip_tags($model->{$field});
        }
    }
}
