<?php
/**
 * Created by PhpStorm.
 * User: m.kuchta
 * Date: 08.04.2019
 * Time: 15:00
 */

namespace App\Helpers;

/**
 * Klasa odpowiada za generowanie tabeli z wynikami.
 * @package App\Helpers
 */
class Grid
{
    private $cols; /** @var \Illuminate\Support\Collection kolekcja zawierająca listę kolumn w tabeli */
    private $rows; /** @var \Illuminate\Support\Collection kolekcja zawierająca listę wierszy w tabeli */
    private $rowActions; /** @var \Illuminate\Support\Collection kolekcja zawierająca liste akcji dla wierszy */
    private $pager; /** @var Pager klasa pozwalająca na wygenerowanie paginacji dla tabeli */

    /**
     * Konstruktor ustawia domyślne wartości dla atrybutów klasy
     * @param Pager|null $pager
     */
    public function __construct(Pager $pager = null)
    {
        $this->cols = collect();
        $this->rows = collect();
        $this->rowActions = collect();
        $this->pager = $pager ?? new Pager();
    }

    /**
     * Metoda wywoływana jest w momencie nieużycia zwrócenia tekstu podczas próby wyświetlenia obiektu.
     * @see renderGrid()
     * @return array|string kod HTML zwrócony przez metodę renderGrid
     */
    public function __toString()
    {
        return $this->renderGrid();
    }

    /**
     * Dodaje kolumnę do kolekcji
     * @param $colName nazwa kolumny
     * @param $text wartość kolumny
     * @param null $class klasa CSS kolumny
     * @return Grid $this
     */
    public function addCol($colName, $text, $class = null)
    {
        $col = collect();
        $col->put('name', $colName);
        $col->put('text', $text);
        $col->put('class', $class);

        $this->cols->put($colName, $col);

        return $this;
    }

    /**
     * Dodaje wiersz do kolekcji
     * @param $entity model lub kolekcja danych
     * @return Grid $this
     */
    public function addRow($entity)
    {
        $this->rows->add($entity);

        return $this;
    }

    /**
     * Dodaje listę wierszy
     * @param $rows tablica modeli lub kolekcji do wstawienia
     * @return Grid $this
     */
    public function addResults($rows)
    {
        foreach($rows as $row)
        {
            $this->addRow($row);
        }

        return $this;
    }

    /**
     * Dodaje akcję dla wierszy do kolekcji
     * @param $url adres URL akcji
     * @param $name nazwa akcji
     * @param $text opis akcji
     * @param null $icon ikona akcji
     * @param array $params - parametry do linku
     * @return Grid $this
     */
    public function addAction($url, $name, $text, $icon = null, $params = [])
    {
        $row = collect();
        $row->put('url', $url);
        $row->put('text', $text);
        $row->put('icon', $icon);
        $row->put('params', $params);

        $this->rowActions->put($name, $row);

        return $this;
    }

    /**
     * Przypisuje obiekt klasy Pager do atrybutu klasy
     * @param Pager $pager
     * @return Grid $this
     */
    public function addPager(Pager $pager)
    {
        $this->pager = $pager;

        return $this;
    }

    /**
     * Zwraca obiekt klasy Pager lub null gdy nie został dodany obiekt
     * @return Pager|null
     */
    public function getPager()
    {
        return $this->pager;
    }

    /**
     * Metoda pozwala na modyfikację dodanych wierszy danych przy użyciu wywołania funkcji zwrotnej
     * @param $callback funkcja zwrotna, która będzie wywołana dla każdego wiersza
     * @return Grid $this
     */
    public function rowsMap($callback)
    {
        foreach($this->rows as $key => $row)
        {
            $this->rows[$key] = $callback($row);
        }

        return $this;
    }

    /**
     * Zwraca kod HTML dla całej tabeli
     * @return string kod HTML
     */
    public function renderGrid()
    {
        if(!$this->pager)
        {
            $this->pager = new Pager();
        }

        return view('partials/grid/grid', ['cols' => $this->cols, 'rows' => $this->rows, 'rowActions' => $this->rowActions, 'pager' => $this->pager])->render();
    }
}
