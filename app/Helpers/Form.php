<?php


namespace App\Helpers;


use Carbon\Carbon;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;

/**
 * Klasa Form odpowiada za generowanie pól formularza
 * @uses Illuminate\Support\Arr
 * @uses Illuminate\Support\Str
 * @package App\Helpers
 */
class Form
{
    private static $tabsCount = 0;
    private static $tabsNames = [];
    private static $activeTab = null;

    /**
     * Zwraca kod HTML wiersza do którego wstawiamy pola formularza
     * @param $html
     * @return string kod HTML
     */
    public static function group($html)
    {
        if (empty($html))
            return '';

        return '
            <div class="row">
                <div class="col-sm-12 col-md-4 form-group">
                    ' . $html . '
                </div>
            </div>
        ';
    }

    /**
     * Zwraca kod HTML dla pola o podanym typie
     * @param $type typ pola
     * @param $name nazwa pola
     * @param $label opis pola
     * @param string $value wartość pola
     * @param bool $required jeżeli TRUE to pole jest wymagane
     * @param bool $disabled jeżeli TRUE to pole jest wyłączone
     * @return string kod HTML
     */
    public static function input($type, $name, $label, $value = '', $required = false, $disabled = false)
    {
        if ($type == 'textarea') {
            return self::textarea($name, $label, $value, $required, $disabled);
        }

        if ($type == 'editor') {
            return self::editor($name, $label, $value, $required, $disabled);
        }

        if (!in_array($type, ['text', 'password', 'email', 'number']))
            return '';

        $disabledTxt = $disabled ? 'disabled="disabled"' : '';
        $requiredTxt = $required ? '<var class="required"></var>' : '';

        return '
            <label for="' . $name . '">' . $label . $requiredTxt . '</label>
            <input type="' . $type . '" name="' . $name . '" class="form-control" placeholder="' . $label . '" value="' . $value . '" ' . $disabledTxt . ' />
        ';
    }

    /**
     * Zwraca kod HTML dla pola typu Textarea
     * @param $name nazwa pola
     * @param $label opis opla
     * @param string $value wartość pola
     * @param bool $required jeżeli TRUE to pole jest wymagane
     * @param bool $disabled jeżeli TRUE to pole jest wyłączone
     * @return string kod HTML
     */
    public static function textarea($name, $label, $value = '', $required = false, $disabled = false)
    {
        $disabledTxt = $disabled ? 'disabled="disabled"' : '';
        $requiredTxt = $required ? '<var class="required"></var>' : '';

        return '
            <label for="' . $name . '">' . $label . $requiredTxt . '</label>
            <textarea name="' . $name . '" class="form-control" placeholder="' . $label . '" ' . $disabledTxt . ' >' . $value . '</textarea>
        ';
    }

    /**
     * Zwraca kod HTML dla pola typu Textarea
     * @param $name nazwa pola
     * @param $label opis opla
     * @param string $value wartość pola
     * @param bool $required jeżeli TRUE to pole jest wymagane
     * @param bool $disabled jeżeli TRUE to pole jest wyłączone
     * @return string kod HTML
     */
    public static function editor($name, $label, $value = '', $required = false, $disabled = false)
    {
        $disabledTxt = $disabled ? 'disabled="disabled"' : '';
        $requiredTxt = $required ? '<var class="required"></var>' : '';

        return '
            <label for="' . $name . '">' . $label . $requiredTxt . '</label>
            <textarea name="' . $name . '" id="jq-' . $name . '" class="form-control" placeholder="' . $label . '" ' . $disabledTxt . ' >' . $value . '</textarea>
            <script>
                $(document).ready(function(){
                    tinymce.init({
                        selector: "#jq-' . $name . '"
                      });
                });
            </script>
        ';
    }

    /**
     * Zwraca kod HTML dla kontenera z możliwością wgrywania zdjęć metodę Drag&Drop
     * @param string $name Nazwa kontenera
     * @param string $label Opis pola
     * @param $uploadUrl string Adres url służący do wgrania zdjęć na serwer
     * @param array $default Domyślne zdjęcia zapisane w bazie
     * @param bool $required Określenie wymagalności pola
     * @return string kod HTML
     */
    public static function uploader($name, $label, $uploadUrl, array $default = [], $required = false)
    {
        $requiredTxt = $required ? '<var class="required"></var>' : '';
        return '
            <label for="' . $name . '">' . $label . $requiredTxt . '</label>
            <input type="hidden" id="' . $name . '" name="' . $name . '"  value="' . json_encode(array_column($default, 'id')) . '"/>
            <div id="dropzone_' . $name . '" class="dropzone"></div>
            <script>
                Dropzone.autoDiscover = false;               
                $(document).ready(function(){
                    $("#dropzone_' . $name . '").dropzone({
                        init: function(){
                            let images = JSON.parse(\'' . json_encode($default) . '\');
                            if (images) {
                                for (var i = 0; i < images.length; i++) {
                                    var mockFile = { 
                                        name: images[i].name, 
                                        size: images[i].size, 
                                        type: images[i].type, 
                                        status: Dropzone.ADDED, 
                                        url: images[i].url
                                    };
                    
                                    // Call the default addedfile event handler
                                    this.emit("addedfile", mockFile);
                                    
                                    // And optionally show the thumbnail of the file:
                                    this.emit("thumbnail", mockFile, images[i].url);
                                    
                                    $("#dropzone_' . $name . '").find(".dz-preview").last().attr("id", images[i].id);
                                    if(images[i].width > images[i].height)
                                    {
                                        $("div#dropzone_' . $name . ' .dz-image").last().find("img").attr("height", "120");
                                    }
                                    else
                                    {
                                        $("div#dropzone_' . $name . ' .dz-image").last().find("img").attr("width", "120");
                                    }
                                    this.files.push(mockFile);
                                    $("#dropzone_' . $name . '").find(".dz-progress").remove();
                                }
                            }
                        
                            this.on("success", function(file, data){
                                let ids = $("#' . $name . '").val();
                                
                                ids = JSON.parse(ids);
                                ids.push(data.id);
                                ids = JSON.stringify(ids);
                                
                                file.previewElement.id = data.id;
                                
                                $("div#dropzone_' . $name . ' .dz-image").last().find("img").attr("src", "/storage/photos/" + data.image);
                                if(data.width > data.height)
                                {
                                    $("div#dropzone_' . $name . ' .dz-image").last().find("img").attr("height", "120");
                                }
                                else
                                {
                                    $("div#dropzone_' . $name . ' .dz-image").last().find("img").attr("width", "120");
                                }
                                $("#' . $name . '").val(ids);
                            });
                            
                            this.on("error", function(file, response) {
                                $(file.previewElement).find(".dz-error-message").text(response);
                            });
                            
                            this.on("removedfile", function(a){
                                let id = $(a.previewElement).attr("id");
                                
                                let ids = $("#' . $name . '").val();
                                
                                ids = JSON.parse(ids);
                                ids = ids.filter(function(el){
                                    return el != id;
                                });
                                ids = JSON.stringify(ids);
                                
                                $("#' . $name . '").val(ids);
                            });
                        },
                        url: "' . $uploadUrl . '",
                        headers: {
                            "X-CSRF-TOKEN": "' . csrf_token() . '"
                        },
                        addRemoveLinks: true,
                        maxFilesize: 20,
                        acceptedFiles: "image/*",
                        clickable: true,
                        createImageThumbnails: true,
                        dictDefaultMessage: "' . __('Kliknij tutaj lub przeciągnij tutaj zdjęcia') . '",
                        dictFallbackMessage: "' . __('Twoja przeglądarka nie wspiera tego typu wgrywania zdjęć') . '",
                        dictFileTooBig: "' . __('Plik jest zbyt duży ({{filesize}}MiB). Max rozmiar: {{maxFilesize}}MiB.') . '",
                        dictInvalidFileType: "' . __('Tego typu pliki są niedozwolone') . '",
                        dictResponseError: "' . __('Serwer odpowiedział kodem {{statusCode}}') . '",
                        dictCancelUpload: "' . __('Anuluj wgrywanie.') . '",
                        dictUploadCanceled: "' . __('Wgrywanie anulowane.') . '",
                        dictCancelUploadConfirmation: "' . __('Jesteś pewien, że chcesz anulować wgrywanie pliku?') . '",
                        dictRemoveFile: "' . __('Usuń plik') . '",
                        dictRemoveFileConfirmation: null,
                        dictMaxFilesExceeded: "' . __('Nie możesz wgrać więcej plików.') . '",
                    });
                });
                
            </script>
        ';
    }

    /**
     * Zwraca kod HTML dla pola jednokrotnego wyboru
     * @param string $name nazwa pola
     * @param string $label opis pola
     * @param bool $checked jeżeli TRUE to pole jest wymagane
     * @param bool $required jeżeli TRUE to pole jest wymagane
     * @param bool $clear
     * @return string kod HTML
     */
    public static function checkbox($name, $label, $checked = false, $required = false, $clear = false)
    {
        $checkedTxt = $checked ? 'checked="checked"' : '';
        $requiredTxt = $required ? '<var class="required"></var>' : '';

        if ($clear)
            return '
                <div class="checkbox">
                    <input type="checkbox" name="' . $name . '" class="form-control" value="1" ' . $checkedTxt . ' />
                    <var></var>
                    <label for="' . $name . '">' . $label . $requiredTxt . '</label>
                </div>
            ';
        else
            return '
                <div class="row">
                    <div class="col-sm-12 col-md-4 form-group checkbox">
                        <input type="checkbox" name="' . $name . '" class="form-control" value="1" ' . $checkedTxt . ' />
                        <var></var>
                        <label for="' . $name . '">' . $label . $requiredTxt . '</label>
                    </div>
                </div>
            ';
    }

    /**
     * Zwraca kod HTML dla przycisków lub linków
     * @param string $type typ przyciski
     * @param string $value wartość przycisku
     * @param string $class klasa CSS
     * @param string $href adres URL na który ma nas przekierować link
     * @param bool $disabled jeżeli TRUE to pole jest wyłączone
     * @return string kod HTML
     */
    public static function button($type = 'submit', $value = '', $class = 'btn btn-primary', $href = '', $disabled = false)
    {
        if ($type == 'link') {
            return '<a href="' . $href . '" class="' . $class . '">' . $value . '</a>';
        } elseif ($type == 'button') {
            return '<button class="' . $class . '">' . $value . '</button>';
        } else {
            return '<input type="submit"  value="' . $value . '" class="' . $class . '" />';
        }
    }

    /**
     * Zwraca kod HTML dla listy przycisków
     * @param array $btns lista danych do wygenerowania przycisków
     * @return string kod HTML
     */
    public static function buttons($btns = [])
    {
        if (empty($btns))
            return '';

        $html = '';
        foreach ($btns as $btn) {
            $type = Arr::get($btn, 'type', 'submit');
            $value = Arr::get($btn, 'value', '');
            $class = Arr::get($btn, 'class', 'btn btn-primary');
            $href = Arr::get($btn, 'href', '');
            $disabled = Arr::get($btn, 'disabled', false);
            $html .= self::button($type, $value, $class, $href, $disabled) . "&nbsp;&nbsp;";
        }

        return $html;
    }

    /**
     * Zwraca kod HTML reprezentujący rozpoczęcie formulrza
     * @param string $action akcja formularza
     * @param string $method metoda przy pomocy której ma zostać przesłany formularz
     * @return string kod HTML
     */
    public static function open($action = '', $method = 'GET')
    {
        $action = !empty($action) ? "action = '$action'" : '';

        return "<form method='$method' $action>" . csrf_field();
    }

    /**
     * Zwraca kod HTML reprezentujący zakończenie formularza
     * @return string kod HTML
     */
    public static function close()
    {
        if (self::$tabsCount > 0) {
            return "
                </form>
                <script>
                    $(document).ready(function(){
                        $('.tabs__header').on('click', function(){
                            setActivityOfTab(this);
                        });
                        
                        if($('.tabs__header.active-header-tab').length == 0)
                        {
                            console.log(".self::$activeTab.")
                            setActivityOfTab($('.tabs__header').eq(".self::$activeTab."));
                        }
                        
                        function setActivityOfTab(el){
                            $('.tabs__header').removeClass('active-header-tab');
                            $(el).addClass('active-header-tab');
                            
                            let rel = $(el).attr('rel');
                            
                            $('.form-tab').removeClass('active-tab');
                            $('.form-tab').addClass('hidden');
                            $('.form-tab#' + rel).removeClass('hidden');
                            $('.form-tab#' + rel).addClass('active-tab');
                        }
                    });
                </script>
            ";
        }

        return '</form>';
    }

    /**
     * Zwraca kod HTML dla zamknięcia zakładki
     * @return string
     */
    public static function closeTab()
    {
        return "</div>";
    }

    /**
     * Zwraca kod HTML reprezentujacy wiersz z nazwą w osobnej kolumnie
     * @param $label opis wiersza
     * @param $text text do wyświetlenia
     * @return string kod HTML
     */
    public static function row($label, $text)
    {
        return "<div class='row'><div class='col'><strong>$label</strong>" . (!empty($label) ? ':' : '') . "</div><div class='col'>$text</div></div>";
    }

    /**
     * Zwraca kod HTML dla pola typu select 2 pozwalającego na wyszukiwanie w opcjach.
     * @param $name nazwa pola
     * @param $label opis pola
     * @param array $options dostępne opcje
     * @param array $selected zaznaczone opcje
     * @param string $ajaxLink adres URL pozwalający na dynamiczne filtrowanie listy
     * @param bool $required jeżeli TRUE pole jest wymagane
     * @param bool $multiple jeżeli TRUE pole jest wielokrotnego wyboru
     * @param string $customAjax podajemy dodatkowy kod dla danych przesłanych przez ajaxa
     * @param string $placeholder zastępczy tekst dla pustej wartości pola
     * @return string kod HTML
     */
    public static function select2($name, $label, $options = [], $selected = [], $ajaxLink = '', $required = false, $multiple = true, $customAjax = '', $placeholder = '')
    {
        $requiredTxt = $required ? '<var class="required"></var>' : '';
        $multipleTxt = $multiple ? 'multiple="multiple"' : '';

        $r = '
            <label for="' . $name . '">' . $label . $requiredTxt . '</label>
            <select name="' . $name . ($multiple ? '[]' : '') . '" id="jq-' . $name . '-select2" ' . $multipleTxt . '>';

        foreach ($options as $key => $option) {
            $selectedStr = in_array($key, $selected) ? 'selected="selected"' : '';
            $r .= '<option value="' . $key . '" ' . $selectedStr . '>' . $option . '</option>';
        }

        $r .= '</select>';

        $r .= '
            <script>
                $(document).ready(function(){
                    $("#jq-' . $name . '-select2").select2({
                        ' . (!empty($ajaxLink) ? '
                        ajax: {
                            url: "' . $ajaxLink . '",
                            processResults: function (data) {
                                return {
                                    results: $.map(data, function(el, key){
                                        return {
                                            "text": el.text,
                                            "id": el.id
                                        };
                                    })
                                };
                            }
                            ' . (!empty($customAjax) ? ',
                        ' . $customAjax : '') . '
                        },
                        ' : '') . '
                        
                        ' . (!empty($placeholder) ? '
                        placeholder: "' . $placeholder . '",
                        ' : '') . '
                        
                        templateResult: function(repo){
                            if (repo.loading) {
                                return repo.text;
                            }
                            return repo.text;
                        }
                    });
                });
            </script>
        ';

        return $r;
    }

    /**
     * Zwraca kod HTML pola typu datepicker, aby można było wybierać datę z kalendarza
     * @param string $type typ pola
     * @param string $name nazwa pola
     * @param string $label opis pola
     * @param string $value domyślna wartość pola
     * @param bool $required czy pole jest wymagane
     * @param bool $disabled czy pole jest wyłączone
     * @return string
     */
    public static function datepicker($type, $name, $label, $value = '', $required = false, $disabled = false)
    {
        $requiredTxt = $required ? '<var class="required"></var>' : '';
        $disabledTxt = $disabled ? 'disabled="disabled"' : '';

        $value = !empty($value) ? Carbon::create($value)->toDateString() : '';

        $r = '
            <label for="' . $name . '">' . $label . $requiredTxt . '</label>
            <input type="text" name="' . $name . '" id="jq-' . $name . '" class="form-control" data-provide="datepicker" placeholder="' . $label . '" value="' . $value . '" ' . $disabledTxt . ' />
            <script>$(document).ready(function(){
                $("#jq-' . $name . '").datepicker({
                    dateFormat: \'yy-mm-dd\',
                    monthNames: ["Styczeń", "Luty", "Marzec", "Kwiecień", "Maj", "Czerwiec", "Lipiec", "Sierpień", "Wrzesień", "Październik", "Listopad", "Grudzień"],
                    dayNamesMin: [ "Nd", "Po", "Wt", "Śr", "Czw", "Pt", "So" ],
                    beforeShow: function() {
                        setTimeout(function(){
                            $("#jq-' . $name . '").css("z-index", 11);
                        }, 0);
                    }
                });
            })</script>';

        return $r;
    }

    /**
     * Zwraca kod HTML dla nagłówka z zakładkami
     * @param array $tabs tablica z zakładkami
     * @return string
     */
    public static function addTabs($tabs = [])
    {
        $return = '<div class="tabs">';

        foreach($tabs as $tab)
        {
            self::$tabsNames[] = Str::slug($tab);
            $return .= '<div class="tabs__header" rel="tab-'.Str::slug($tab).'">'.$tab.'</div>';
        }

        return $return.'</div>';
    }

    /**
     * Dodaje nową zakładkę oraz zwraca kod HTML zamknięcia poprzedniej zakładki i otwarcia nowej
     * @param string $name nazwa zakładki
     * @param bool $isActive czy zakładka ma zostać uruchomiona jako pierwsza
     * @return string
     */
    public static function addTab($name, $isActive = false)
    {
        if(count(self::$tabsNames) == 0)
            return '';

        $return = '';

        if(is_null(self::$activeTab) || $isActive)
        {
            self::$activeTab = array_search(Str::slug($name), self::$tabsNames);
        }

        if(self::$tabsCount > 0)
        {
            $return .= self::closeTab();
        }

        $return .= '<div class="form-tab hidden" id="tab-'.Str::slug($name).'">';

        self::$tabsCount++;
        return $return;
    }
}
