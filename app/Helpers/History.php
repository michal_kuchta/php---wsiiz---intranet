<?php
/**
 * Created by PhpStorm.
 * User: m.kuchta
 * Date: 08.04.2019
 * Time: 15:00
 */

namespace App\Helpers;

use http\Exception\InvalidArgumentException;
use Illuminate\Database\Eloquent\Model;

/**
 * Klasa odpowiada za generowanie tabeli z historią modelu.
 * @package App\Helpers
 */
class History
{
    /**
     * Zwraca kod HTML zakładki z historią modelu
     * @param Model $entity obiekt modelu z danymi
     * @return string
     */
    public static function render($entity)
    {
        if(!($entity instanceof Model))
            throw new InvalidArgumentException(__('Podany model powinien być typu Model'));

        $history = $entity->histories()->orderBy('performed_at', 'desc')->take(20)->get();

        return view('partials/history/index', ['histories' => $history]);
    }
}
