<?php


namespace App\Helpers;

/**
 * Interfejs IAuditable pozwala nam na sprawdzenie czy dany model ma używać obserwatora AuditableObserver
 * @see AuditableObserver
 * @package App\Helpers
 */
interface IAuditable
{

}
