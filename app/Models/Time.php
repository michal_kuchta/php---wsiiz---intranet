<?php

namespace App\Models;

use App\Helpers\AuditableObserver;
use App\Helpers\IAuditable;
use App\Helpers\StripTagsObserver;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Arr;
use Panoscape\History\HasHistories;

/**
 * Model projektów
 * @uses App\Helpers\AuditableObserver
 * @uses App\Helpers\IAuditable
 * @uses App\Helpers\StripTagsObserver
 * @uses Illuminate\Database\Eloquent\Model
 * @uses Illuminate\Support\Arr
 * @uses Panoscape\History\HasHistories
 * @package App\Models
 * @package IAuditable
 */
class Time extends Model implements IAuditable
{
    use HasHistories;

    protected $table = 'times';

    protected $fillable = ['project_id', 'task_id', 'user_id', 'description', 'date', 'hours'];

    private $fieldsNames = [];

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->fieldsNames = [
            'project_id' => __('Id projektu'),
            'task_id' => __('Id zadania'),
            'user_id' => __('Id użytkownika'),
            'description' => __('Opis'),
            'date' => __('Data'),
            'hours' => __('Ilość godzin')
        ];
    }

    /**
     *  Metoda pobiera nazwę pola przy użyciu klucza
     * @param $key
     * @return string
     */
    public function getFieldName($key)
    {
        return Arr::get($this->fieldsNames, $key, $key);
    }

    public static function boot()
    {
        parent::boot();

        self::observe(new AuditableObserver());
        self::observe(new StripTagsObserver(['description', 'date', 'hours']));
    }

    /**
     * Zwraca realację jeden do jeden z modelem Project
     * @see Project
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function project()
    {
        return $this->hasOne(Project::class, 'id', 'project_id');
    }

    /**
     * Zwraca realację jeden do jeden z modelem Task
     * @see Task
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function task()
    {
        return $this->hasOne(Task::class, 'id', 'task_id');
    }

    /**
     * Zwraca realację jeden do jeden z modelem Task
     * @see Task
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function user()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }

    /**
     * Zwraca nazwę modelu dla historii
     * @return array|string|null
     */
    public function getModelLabel()
    {
        return '';
    }
}