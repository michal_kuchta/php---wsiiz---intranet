<?php

namespace App\Models;

use App\Helpers\AuditableObserver;
use App\Helpers\IAuditable;
use App\Helpers\StripTagsObserver;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Arr;
use Panoscape\History\HasHistories;
use Silber\Bouncer\Database\HasRolesAndAbilities;

/**
 * Model pracowników
 * @uses App\Helpers\AuditableObserver
 * @uses App\Helpers\IAuditable
 * @uses App\Helpers\StripTagsObserver
 * @uses Illuminate\Notifications\Notifiable
 * @uses Illuminate\Foundation\Auth\User
 * @uses Illuminate\Support\Arr
 * @uses Panoscape\History\HasHistories
 * @uses Silber\Bouncer\Database\HasRolesAndAbilities
 * @package App\Models
 * @package IAuditable
 */
class User extends Authenticatable implements IAuditable
{
    use Notifiable;
    use HasRolesAndAbilities;
    use HasHistories;

    /**
     * Atrybuty które mogą zostać wypełnione
     *
     * @var array
     */
    protected $fillable = [
        'username', 'email', 'password', 'is_active', 'position', 'first_name', 'last_name', 'is_admin'
    ];

    /**
     * Atrybuty które nie będą wyświetlane w modelu
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    private $fieldsNames = [];

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->fieldsNames = [
            'email' => __('Adres e-mail'),
            'is_active' => __('Czy aktywny'),
            'position' => __('Stanowisko'),
            'first_name' => __('Imię'),
            'last_name' => __('Nazwisko'),
            'is_admin' => __('Czy admin'),
        ];
    }

    /**
     *  Metoda pobiera nazwę pola przy użyciu klucza
     * @param $key
     * @return string
     */
    public function getFieldName($key)
    {
        return Arr::get($this->fieldsNames, $key, $key);
    }

    public static function boot()
    {
        parent::boot();

        self::observe(new AuditableObserver()); /** Uruchomienie obeserwatora */
        self::observe(new StripTagsObserver(['username', 'email', 'position', 'first_name', 'last_name']));
    }

    /**
     * Zwraca wartość logiczną sprawdzając czy pracownik jest aktywny
     * @return bool
     */
    public function isActive()
    {
        return (int)$this->is_active === 1;
    }

    /**
     * Zwraca wartość logiczną sprawdzając czy pracownik jest administratorem
     * @return bool
     */
    public function isAdmin()
    {
        return (int)$this->is_admin === 1;
    }

    /**
     * Zwraca imię i nazwisko pracownika
     * @return string
     */
    public function getName()
    {
        return $this->first_name . ' ' . $this->last_name;
    }

    /**
     * Zwraca relację jeden do wielu do modelu ProjectManager
     * @see ProjectManager
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function projects()
    {
        return $this->hasMany(ProjectManager::class, 'user_id', 'id');
    }

    /**
     * Zwraca relację jeden do wielu do modelu ProjectWorker
     * @see ProjectWorker
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function projectsWorker()
    {
        return $this->hasMany(ProjectWorker::class, 'user_id', 'id');
    }

    /**
     * Zwraca listę projektów przypisanych do pracownika
     * @return array
     */
    public function getProjects()
    {
        $ret = [];
        $this->load(['projects', 'projectsWorker']);
        foreach($this->projects as $project)
        {
            $ret[$project->project->id] = $project;
        }

        foreach($this->projectsWorker as $project)
        {
            $ret[$project->project->id] = $project;
        }

        return $ret;
    }

    /**
     * Zwraca listę zadań przypisanych do pracownika
     * @return array
     */
    public function getTasks()
    {
        $ret = [];
        $tasks = Task::query()->where('signed_to', '=', \Auth::id())->orderBy('created_at')->limit(5)->get();

        foreach($tasks as $task)
        {
            $ret[$task->id] = $task;
        }

        return $ret;
    }

    /**
     * Sprawdza uprawnienia danego pracownika
     * @param $name
     * @return bool
     */
    public function checkPrivileges($name)
    {
        return $this->can($name) || $this->isAdmin();
    }

    /**
     * Zwraca nazwę modelu dla historii
     * @return array|string|null
     */
    public function getModelLabel()
    {
        return $this->getName();
    }
}
