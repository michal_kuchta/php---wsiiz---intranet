<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Model dla zdjęć
 * @uses Illuminate\Database\Eloquent\Model
 * @package App\Models
 */
class Images extends Model
{
    protected $table = 'images';

    protected $fillable = [
        'url'
    ];
}
