<?php

namespace App\Models;

use App\Helpers\AuditableObserver;
use App\Helpers\IAuditable;
use App\Helpers\StripTagsObserver;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Arr;
use Panoscape\History\HasHistories;

/**
 * Model projektów
 * @uses App\Helpers\AuditableObserver
 * @uses App\Helpers\IAuditable
 * @uses App\Helpers\StripTagsObserver
 * @uses Illuminate\Database\Eloquent\Model
 * @uses Illuminate\Support\Arr
 * @uses Panoscape\History\HasHistories
 * @package App\Models
 * @package IAuditable
 */
class Project extends Model implements IAuditable
{
    use HasHistories;
    protected $table = 'projects';

    protected $fillable = ['name', 'description', 'is_active', 'is_public', 'git_repository_name', 'budget', 'hourly_rate'];

    public $v_managers = [];
    public $v_workers = [];

    private $fieldsNames = [];

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->fieldsNames = [
            'name' => __('Nazwa'),
            'description' => __('Opis'),
            'is_active' => __('Czy aktywny'),
            'is_public' => __('Czy publiczny'),
            'git_repository_name' => __('Link do repozytorium'),
            'budget' => __('Budzet'),
            'hourly_rate' => __('Ilość godzin')
        ];
    }

    /**
     *  Metoda pobiera nazwę pola przy użyciu klucza
     * @param $key
     * @return string
     */
    public function getFieldName($key)
    {
        return Arr::get($this->fieldsNames, $key, $key);
    }

    public static function boot()
    {
        parent::boot();

        self::observe(new AuditableObserver());
        self::observe(new ProjectsObserver());
        self::observe(new StripTagsObserver(['name', 'git_repository_name', 'budget', 'hourly_rate']));
    }

    /**
     * Zwraca relację jeden do wielu do modelu ProjectManager
     * @see ProjectManager
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function managers()
    {
        return $this->hasMany(ProjectManager::class, 'project_id', 'id');
    }

    /**
     * Zwraca listę kierowników projektu
     * @return array
     */
    public function managersList()
    {
        $options = [];

        if(!empty($this->v_managers))
        {
            $managers = User::query()->whereIn('id', $this->v_managers, 'or')->get();
            foreach($managers as $manager)
            {
                $options[$manager->id] = $manager->getName();
            }
        }
        else
        {
            foreach($this->managers as $manager)
            {
                $options[$manager->user_id] = $manager->manager->getName();
            }
        }

        return $options;
    }

    /**
     * Zwraca relację jeden do wielu do modelu ProjectWorker
     * @see ProjectWorker
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function workers()
    {
        return $this->hasMany(ProjectWorker::class, 'project_id', 'id');
    }

    /**
     * Zwraca listę pracowników projektu
     * @return array
     */
    public function workersList()
    {
        $options = [];

        if(!empty($this->v_workers))
        {
            $workers = User::query()->whereIn('id', $this->v_workers, 'or')->get();
            foreach($workers as $worker)
            {
                $options[$worker->id] = $worker->getName();
            }
        }
        else
        {
            $workers = $this->workers;
            foreach($workers as $worker)
            {
                $options[$worker->user_id] = $worker->worker->getName();
            }
        }

        return $options;
    }

    /**
     * Zwraca relację jeden do wielu do modelu ProjectTask
     * @see ProjectTask
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function tasks()
    {
        return $this->hasMany(ProjectTask::class, 'project_id', 'id');
    }

    /**
     * Zwraca relację jeden do wielu do modelu Time
     * @see Time
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function times()
    {
        return $this->hasMany(Time::class, 'project_id', 'id');
    }

    /**
     * Zwraca łączną ilość godzin dla projektu
     */
    public function getTotalHours()
    {
        $hours = 0;
        $fullHours = 0;

        if($this->times)
        {
            foreach($this->times as $row)
            {
                $hours += (float)$row->hours;
            }
        }

        if($this->tasks)
        {
            foreach($this->tasks as $task)
            {
                $fullHours += (float)$task->task->hours;
            }
        }

        if($fullHours > 0)
            return str_replace('.', ',', $hours) . ' / ' . str_replace('.', ',', $fullHours);

        return str_replace('.', ',', $hours);
    }

    /**
     * Zwraca wartość logiczną sprawdzając czy projekt jest aktywny
     * @return bool
     */
    public function isActive()
    {
        return (int)$this->is_active === 1;
    }

    /**
     * Zwraca wartość logiczną sprawdzając czy projekt jest publiczny
     * @return bool
     */
    public function isPublic()
    {
        return (int)$this->is_public === 1;
    }

    /**
     * Zwraca nazwę modelu dla historii
     * @return array|string|null
     */
    public function getModelLabel()
    {
        return $this->name;
    }
}

/**
 * Obserwator, który uruchamia metody podczas zapisywania oraz modyfikowania modelu
 * @package App\Models
 */
class ProjectsObserver
{
    /**
     * Metoda przetwarza pracowników oraz kierowników i zapisuje ich w osobnych tabelach. Metoda uruchamia się podczas zapisywania zmian w obiekcie
     * @param IAuditable $model
     */
    public function saved(IAuditable $model)
    {
        if(!empty($model->v_managers))
        {
            $this->processWorkers(ProjectManager::class, $model->v_managers, $model);
        }

        if(!empty($model->v_managers))
        {
            $this->processWorkers(ProjectWorker::class, $model->v_workers, $model);
        }
    }

    /**
     * Metoda przetwarza listę pracowników z odpowiedniego modelu
     * @param $class
     * @param $entities
     * @param IAuditable $project
     */
    public function processWorkers($class, $entities, IAuditable $project)
    {
        $class::query()->where('project_id', '=', $project->id)->delete();
        foreach($entities as $entity)
        {
            $obj = new $class();
            $obj->user_id = $entity;
            $obj->project_id = $project->id;
            $obj->save();
        }
    }

}