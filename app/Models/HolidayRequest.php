<?php

namespace App\Models;

use App\Helpers\StripTagsObserver;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Arr;
use Panoscape\History\HasHistories;

/**
 * Model wniosków urlopowych
 * @uses App\Helpers\StripTagsObserver
 * @uses Illuminate\Database\Eloquent\Model
 * @uses Illuminate\Support\Arr
 * @uses Panoscape\History\HasHistories
 * @package App\Models
 */
class HolidayRequest extends Model
{
    use HasHistories;
    protected $table = 'holiday_requests';

    protected $fillable = ['user_id', 'date_from', 'date_to', 'description', 'status', 'type'];

    private $fieldsNames = [];

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->fieldsNames = [
            'user_id' => __('Id pracownika'),
            'date_from' => __('Data rozpoczęcia urlopu'),
            'date_to' => __('Data zakończenia urlopu'),
            'description' => __('Treść'),
            'status' => __('Status wniosku'),
            'type' => __('Typ wniosku')
        ];
    }

    /**
     *  Metoda pobiera nazwę pola przy użyciu klucza
     * @param $key
     * @return string
     */
    public function getFieldName($key)
    {
        return Arr::get($this->fieldsNames, $key, $key);
    }

    protected static function boot()
    {
        parent::boot();
        self::observe(new StripTagsObserver(['date_from', 'date_to', 'description']));
    }

    /**
     * Zwraca tablicę statusów wniosków urlopowych
     * @return array
     */
    public function getStatuses()
    {
        return [
            1 => __('Do zatwierdzenia'),
            2 => __('Zatwierdzony'),
            3 => __('Odrzucony')
        ];
    }

    /**
     * Pobiera nazwę statusu po jego kluczu
     * @param $key
     * @return mixed
     */
    public function getStatus($key)
    {
        return \Arr::get($this->getStatuses(), $key, '');
    }

    /**
     * Zwraca tablicę typów wniosków urlopowych
     * @return array
     */
    public function getTypes()
    {
        return [
            1 => __('Urlop'),
        ];
    }

    /**
     * Pobiera nazwę typu wniosku po jego kluczu
     * @param $key
     * @return mixed
     */
    public function getType($key)
    {
        return \Arr::get($this->getTypes(), $key, '');
    }

    /**
     * Zwraca relację jeden do jeden do modelu Users
     * @see User
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function user()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }

    /**
     * Zwraca nazwę modelu dla historii
     * @return array|string|null
     */
    public function getModelLabel()
    {
        return '';
    }
}
