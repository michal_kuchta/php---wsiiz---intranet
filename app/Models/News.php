<?php

namespace App\Models;

use App\Helpers\AuditableObserver;
use App\Helpers\IAuditable;
use App\Helpers\StripTagsObserver;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Arr;
use Panoscape\History\HasHistories;

/**
 * Model projektów
 * @uses App\Helpers\AuditableObserver
 * @uses App\Helpers\IAuditable
 * @uses App\Helpers\StripTagsObserver
 * @uses Illuminate\Database\Eloquent\Model
 * @uses Illuminate\Support\Arr
 * @uses Panoscape\History\HasHistories
 * @package App\Models
 * @package IAuditable
 */
class News extends Model implements IAuditable
{
    use HasHistories;

    protected $table = 'news';

    protected $fillable = ['published_at', 'unpublished_at', 'name', 'content', 'status', 'images', 'author_id'];

    private $fieldsNames = [];

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->fieldsNames = [
            'published_at' => __('Data publikacji'),
            'unpublished_at' => __('Data wyłączenia publikacji'),
            'name' => __('Tytuł'),
            'content' => __('Treść'),
            'status' => __('Status'),
            'images' => __('Lista zdjęć'),
            'author_id' => __('Autor')
        ];
    }

    /**
     *  Metoda pobiera nazwę pola przy użyciu klucza
     * @param $key
     * @return string
     */
    public function getFieldName($key)
    {
        return Arr::get($this->fieldsNames, $key, $key);
    }

    public static function boot()
    {
        parent::boot();

        self::observe(new AuditableObserver());
        self::observe(new StripTagsObserver(['published_at', 'unpublished_at', 'name']));
    }

    /**
     * Zwraca relację 1 do 1 di modelu użytkowników
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function author()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }

    /**
     * Zwraca nazwę modelu dla historii
     * @return array|string|null
     */
    public function getModelLabel()
    {
        return $this->name;
    }
}
