<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Panoscape\History\HasHistories;

/**
 * Model powiązania kierownika oraz projektu
 * @uses Illuminate\Database\Eloquent\Model
 * @uses Panoscape\History\HasHistories
 * @package App\Models
 */
class ProjectManager extends Model
{

    use HasHistories;
    protected $table = 'project_managers';
    public $timestamps = false;

    /**
     * Zwraca realację jeden do jeden z modelem Project
     * @see Project
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function project()
    {
        return $this->hasOne(Project::class, 'id', 'project_id');
    }

    /**
     * Zwraca realację jeden do jeden z modelem Project
     * @see User
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function manager()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }

    /**
     * Zwraca nazwę modelu dla historii
     * @return array|string|null
     */
    public function getModelLabel()
    {
        return __('Menadżerzy projektu');
    }
}
