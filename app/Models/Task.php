<?php

namespace App\Models;

use App\Helpers\AuditableObserver;
use App\Helpers\IAuditable;
use App\Helpers\StripTagsObserver;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Arr;
use Panoscape\History\HasHistories;

/**
 * Model projektów
 * @uses App\Helpers\AuditableObserver
 * @uses App\Helpers\IAuditable
 * @uses App\Helpers\StripTagsObserver
 * @uses Illuminate\Database\Eloquent\Model
 * @uses Illuminate\Support\Arr
 * @uses Panoscape\History\HasHistories
 * @package App\Models
 * @package IAuditable
 */
class Task extends Model implements IAuditable
{
    use HasHistories;

    protected $table = 'tasks';

    protected $fillable = ['signed_to', 'name', 'description', 'status', 'is_private', 'date_start', 'date_end', 'hours'];

    public $v_projectId;
    public $v_signedTo;

    private $fieldsNames = [];

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->fieldsNames = [
            'signed_to' => __('Przypisane do'),
            'name' => __('Nazwa'),
            'description' => __('Opis'),
            'status' => __('Status'),
            'is_private' => __('Czy prywatne'),
            'date_start' => __('Data rozpoczęcia'),
            'date_end' => __('Data zakończenia'),
            'hours' => __('Ilość godzin')
        ];
    }

    /**
     *  Metoda pobiera nazwę pola przy użyciu klucza
     * @param $key
     * @return string
     */
    public function getFieldName($key)
    {
        return Arr::get($this->fieldsNames, $key, $key);
    }

    public static function boot()
    {
        parent::boot();

        self::observe(new AuditableObserver());
        self::observe(new TasksObserver());
        self::observe(new StripTagsObserver(['name', 'date_start', 'date_end', 'hours']));
    }

    /**
     * Zwraca listę statusów zadań
     * @return array
     */
    public function getStatuses()
    {
        $statuses = [
            1 => __('Nowe zadanie'),
            2 => __('Do realizacji'),
            3 => __('Realizowane'),
            4 => __('Zrealizowane'),
            5 => __('Zwrócone'),
            6 => __('Do wgrania'),
            7 => __('Do wyceny'),
            8 => __('Zakończone')
        ];

        return $statuses;
    }

    /**
     * Zwraca realację jeden do jeden z modelem Project
     * @see Project
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function project()
    {
        return $this->hasOne(ProjectTask::class, 'task_id', 'id');
    }

    /**
     * Zwraca listę projektów
     * @return array
     */
    public function getProject()
    {
        if($this->project)
        {
            return [
                $this->project->project_id => $this->project->project->name
            ];
        }
        elseif(!empty($this->v_projectId))
        {
            $r = Project::query()->find($this->v_projectId);
            return [
                $r->id => $r->name
            ];
        }
        return [];
    }

    /**
     * Zwraca wartość logiczną sprawdzając czy projekt jest prywatny
     * @return bool
     */
    public function isPrivate()
    {
        return (int)$this->is_private === 1;
    }

    /**
     * Zwraca realację jeden do jeden z modelem User
     * @see User
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function signedTo()
    {
        return $this->hasOne(User::class, 'id', 'signed_to');
    }

    /**
     * Pobiera listę przypisanych pracowników
     * @return array
     */
    public function getSignedTo()
    {
        if($this->signedTo)
        {
            return [
                $this->signedTo->id => $this->signedTo->getName()
            ];
        }
        elseif(!empty($this->signed_to))
        {
            $r = User::query()->find($this->signed_to);
            return [
                $r->id => $r->getName()
            ];
        }
        return [];
    }

    /**
     * Zwraca relację 1 do 1 do modelu Time
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function times()
    {
        return $this->hasMany(Time::class, 'task_id', 'id');
    }

    /**
     * Zwraca łączną ilość godzin dla projektu
     */
    public function getTotalHours()
    {
        $return = 0;

        if($this->times)
        {
            foreach($this->times as $row)
            {
                $return += (float)$row->hours;
            }
        }

        return str_replace('.', ',', $return);
    }

    /**
     * Zwraca nazwę modelu dla historii
     * @return array|string|null
     */
    public function getModelLabel()
    {
        return $this->name;
    }
}

/**
 * Obserwator, który uruchamia metody podczas zapisywania oraz modyfikowania modelu
 * @package App\Models
 */
class TasksObserver
{
    /**
     * Metoda przetwarza projekty i zapisuje je w osobnej tabeli. Metoda uruchamia się podczas zapisywania zmian w obiekcie
     * @param IAuditable $model
     */
    public function saved(IAuditable $model)
    {
        if((int)$model->v_projectId > 0)
        {
            ProjectTask::query()->where('task_id', '=', $model->id)->delete();
            $obj = new ProjectTask();
            $obj->project_id = $model->v_projectId;
            $obj->task_id = $model->id;
            $obj->save();
        }
    }

    /**
     * Metoda usuwa wszystkie zadania z projektu w momencie usuwania projektu
     * @param IAuditable $model
     */
    public function deleted(IAuditable $model)
    {
        ProjectTask::query()->where('task_id', '=', $model->id)->delete();
    }
}