<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Panoscape\History\HasHistories;

/**
 * Model powiązania projektu oraz zadania
 * @uses Illuminate\Database\Eloquent\Model
 * @uses Panoscape\History\HasHistories
 * @package App\Models
 */
class ProjectTask extends Model
{
    use HasHistories;
    protected $table = 'project_tasks';
    public $timestamps = false;

    /**
     * Zwraca realację jeden do jeden z modelem Project
     * @see Project
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function project()
    {
        return $this->hasOne(Project::class, 'id', 'project_id');
    }

    /**
     * Zwraca realację jeden do jeden z modelem Task
     * @see Task
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function task()
    {
        return $this->hasOne(Task::class, 'id', 'task_id');
    }

    /**
     * Zwraca nazwę modelu dla historii
     * @return array|string|null
     */
    public function getModelLabel()
    {
        return __('Zadania projektu');
    }
}
