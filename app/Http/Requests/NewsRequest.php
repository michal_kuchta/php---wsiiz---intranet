<?php

namespace App\Http\Requests;

use App\Models\User;
use Illuminate\Foundation\Http\FormRequest;

/**
 * Klasa waliduje formularze aktualności
 * @package App\Http\Requests
 */
class NewsRequest extends FormRequest
{
    protected $errorBag = 'errors';

    /**
     * Zwraca tablicę rul walidacji
     * @return array
     */
    public function rules()
    {
        $rules = [
            'name' => 'required|max:240',
            'author_id' => 'required',
        ];

        return $rules;
    }

    /**
     * Zwraca tablicę komuniaktów walidacji
     * @return array
     */
    public function messages()
    {
        return [
            'name.required' => __('Nazwa jest wymagana'),
            'author_id.required' => __('Autor jest wymagany'),
        ];
    }
}
