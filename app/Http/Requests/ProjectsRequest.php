<?php

namespace App\Http\Requests;

use App\Models\User;
use Illuminate\Foundation\Http\FormRequest;

/**
 * Klasa waliduje formularze wniosków urlopowych
 * @package App\Http\Requests
 */
class ProjectsRequest extends FormRequest
{
    protected $errorBag = 'errors';

    /**
     * Zwraca tablicę rul walidacji
     * @return array
     */
    public function rules()
    {
        $rules = [
            'name' => 'required|max:120',
            'description' => 'required',
            'git_repository_name' => 'nullable|regex:/(.*?)\/(.*?)\/(.*?)\.git$/'
        ];

        return $rules;
    }

    /**
     * Zwraca tablicę komuniaktów walidacji
     * @return array
     */
    public function messages()
    {
        return [
            'name.required' => __('Nazwa jest wymagane'),
            'description.required' => __('Opis jest wymagany'),
            'git_repository_name.regex' => __('Niepoprawny adres do projektu git'),
        ];
    }
}
