<?php

namespace App\Http\Requests;

use App\Models\User;
use Illuminate\Foundation\Http\FormRequest;

/**
 * Klasa waliduje formularze czasu pracy
 * @package App\Http\Requests
 */
class TimeRequest extends FormRequest
{
    protected $errorBag = 'errors';

    /**
     * Zwraca tablicę rul walidacji
     * @return array
     */
    public function rules()
    {
        $rules = [
            'project_id' => 'required|exists:projects,id',
            'task_id' => 'required|exists:tasks,id',
            'description' => 'required',
            'date' => 'required',
            'hours' => 'required',
        ];

        return $rules;
    }

    /**
     * Zwraca tablicę komuniaktów walidacji
     * @return array
     */
    public function messages()
    {
        return [
            'project_id.required' => __('Należy wybrać projekt'),
            'project_id.exists' => __('Wybrany projekt nie istnieje w bazie danych lub nie masz uprawnień do tego projektu oraz zadań tego projektu'),
            'task_id.required' => __('Należy wybrać projekt'),
            'task_id.exists' => __('Wybrane zadanie nie istnieje w bazie danych lub nie jest przypisane do wybranego projektu'),
            'description.required' => __('Opis wykonywanych czynności jest wymagany'),
            'date.required' => __('Data jest wymagana'),
            'hours.required' => __('Ilość godzin jest wymagana'),
        ];
    }
}
