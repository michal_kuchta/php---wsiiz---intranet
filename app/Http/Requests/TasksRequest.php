<?php

namespace App\Http\Requests;

use App\Models\User;
use Illuminate\Foundation\Http\FormRequest;

/**
 * Klasa waliduje formularze wniosków urlopowych
 * @package App\Http\Requests
 */
class TasksRequest extends FormRequest
{
    protected $errorBag = 'errors';

    /**
     * Zwraca tablicę rul walidacji
     * @return array
     */
    public function rules()
    {
        $rules = [
            'name' => 'required|max:120',
            'description' => 'required',
            'project_id' => 'required|min:1',
            'date_start' => 'required',
            'date_end' => 'required'
        ];

        return $rules;
    }

    /**
     * Zwraca tablicę komuniaktów walidacji
     * @return array
     */
    public function messages()
    {
        return [
            'name.required' => __('Nazwa jest wymagane'),
            'description.required' => __('Opis jest wymagany'),
            'project_id.required' => __('Należy wybrać projekt'),
            'project_id.min' => __('Należy wybrać projekt'),
            'date_start.required' => __('Data rozpoczęcia jest wymagana'),
            'date_end.required' => __('Data zakończenia jest wymagana'),
        ];
    }
}
