<?php

namespace App\Http\Requests;

use App\Models\User;
use Illuminate\Foundation\Http\FormRequest;

/**
 * Klasa waliduje formularze wniosków urlopowych
 * @package App\Http\Requests
 */
class WorkersRequest extends FormRequest
{
    protected $errorBag = 'errors';

    /**
     * Zwraca tablicę rul walidacji
     * @return array
     */
    public function rules(User $user = null, $mode = 'edit')
    {
        $rules = [
            'first_name' => 'required|max:120',
            'last_name' => 'required|max:120',
            'position' => 'required|max:200',
        ];

        if($user && $user->isDirty('username'))
        {
            $rules['username'] = 'required|unique:users,username';
        }

        if($user && $user->isDirty('email'))
        {
            $rules['email'] = 'required|email|unique:users,email';
        }

        if(!$user)
        {
            $rules['password'] = 'required';
        }

        return $rules;
    }

    /**
     * Zwraca tablicę komuniaktów walidacji
     * @return array
     */
    public function messages()
    {
        return [
            'username.required' => __('Login jest wymagany'),
            'username.unique' => __('Login jest już zajęty'),
            'first_name.required' => __('Imię jest wymagane'),
            'first_name.max' => __('Imię nie może być dłuższe niż :max'),
            'last_name.required' => __('Nazwisko jest wymagane'),
            'last_name.max' => __('Nazwisko nie może być dłuższe niż :max'),
            'email.required' => __('Adres e-mail jest wymagany'),
            'email.email' => __('Niepoprawny adres e-mail'),
            'email.unique' => __('Adres e-mail jest już zajęty'),
            'password.required' => __('Hasło jest wymagane'),
            'position.required' => __('Stanowisko jest wymagane'),
            'position.max' => __('Stanowisko nie może być dłuższe niż :max'),
        ];
    }
}
