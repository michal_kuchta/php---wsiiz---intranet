<?php

namespace App\Http\Middleware;

use Illuminate\Auth\Middleware\Authenticate as Middleware;

/**
 * Klasa odpowiada za uruchomienie procedur dla logowania użytkownika
 * @package App\Http\Middleware
 */
class Authenticate extends Middleware
{
    /**
     * Zwraca adres URL do przekierowania zalogowanego użytkownika
     *
     * @param  \Illuminate\Http\Request  $request
     * @return string
     */
    protected function redirectTo($request)
    {
        if (! $request->expectsJson()) {
            return route('login');
        }
    }
}
