<?php

namespace App\Http\Controllers;

use App\Helpers\Grid;
use App\Helpers\Pager;
use App\Http\Requests\ProjectsRequest;
use App\Models\Project;
use App\Models\Task;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;

/**
 * Klasa odpowiada za obsługę funkcjonalności projektów
 * @uses  App\Helpers\Grid
 * @uses  App\Helpers\Pager
 * @uses  App\Http\Requests\ProjectsRequest
 * @uses  App\Models\Project
 * @uses  App\Models\Task
 * @uses  Illuminate\Http\Request
 * @uses  Illuminate\Support\Arr
 * @package App\Http\Controllers
 */
class ProjectsController extends Controller
{
    /**
     * Wywołanie konstruktora klasy nadrzędnej
     * @see Controller
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Zwraca widok listy projektów
     * @param null|int $page numer strony
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function getIndex($page = null)
    {
        if(!\Auth::user()->checkPrivileges('projects.Index'))
            return view('noAccess');

        $grid = new Grid();

        $grid->addCol('name', __('Nazwa'));
        $grid->addCol('created_at', __('Data utworzenia'), 'hidden-xxs');
        $grid->addCol('tasks', __('Ilość zadań'), 'hidden-sm hidden-xs');
        $grid->addCol('isActive', __('Status aktywności'), 'hidden-xs');
        $grid->addCol('isPublic', __('Status publikacji'), 'hidden-xs');

        if(\Auth::user()->checkPrivileges('projects.Edit'))
        {
            $grid->addCol('hours', __('Ilość godzin'), 'hidden-sm hidden-xs');
            $grid->addCol('budget', __('Budżet'), 'hidden-sm hidden-xs');
            $grid->addAction(route('projects.getEdit'), 'editProject', 'Edytuj', 'fas fa-edit', ['id']);
        }

        if(\Auth::user()->checkPrivileges('projects.Delete'))
            $grid->addAction(route('projects.getDelete'), 'deleteProject', 'Usuń', 'fas fa-trash-alt', ['id']);

        if(\Auth::user()->checkPrivileges('projects.Commits'))
            $grid->addAction(route('projects.getCommits'), 'getCommits', 'Zobacz zmiany w GIT', 'fab fa-git-square', ['id']);

        if(\Auth::user()->checkPrivileges('projects.GanttChart'))
            $grid->addAction(route('projects.getGanttChart'), 'getGanttChart', 'Harmonogram zadań - Gantt', 'fas fa-chart-bar', ['id']);

        $rowCount = Project::query()->count();

        $pager = new Pager($rowCount, 10, $page);
        $pager->setUrl(route('projects.getIndex'));
        $grid->addPager($pager);

        $rows = Project::query()->skip($pager->getOffset())->limit($pager->getLimit())->get();

        $grid->addResults($rows);

        $grid->rowsMap(function($row){
            if(\Auth::user()->checkPrivileges('projects.Edit'))
            {
                $row->hours = $row->getTotalHours();

                $budget = '';
                if(!empty($row->budget) && $row->budget > 0)
                {
                    $budget = $row->budget;
                    if(!empty($row->hourly_rate) && $row->hourly_rate > 0)
                    {
                        $budget = ((float)$row->hours * (float)$row->hourly_rate) . " / " . $budget;
                    }
                }
                $row->budget = $budget;
            }

            $row->tasks = count($row->tasks);
            $row->isActive = '<span class="'.($row->isActive() ? 'statusActive' : 'statusInActive').'">'.($row->isActive() ? 'Aktywny' : 'Nieaktywny').'</span>';
            $row->isPublic = '<span class="'.($row->isPublic() ? 'statusActive' : 'statusInActive').'">'.($row->isPublic() ? 'Publiczny' : 'Niepubliczny').'</span>';
            $row->activeProjects = 0;

            return $row;
        });

        return view('projects/index', ['grid' => $grid]);
    }

    /**
     * Zwraca widok formularza dodawania projektu
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getCreate()
    {
        if(!\Auth::user()->checkPrivileges('projects.Create'))
            return view('noAccess');

        $entity = new Project(request()->old());
        $entity->v_managers = Arr::get(request()->old(), 'managers', []);
        $entity->v_workers = Arr::get(request()->old(), 'workers', []);
        return view('projects/form', ['entity' => $entity]);
    }

    /**
     * Przetwarza przesłany formularz dodawania projektów, waliduje go, jeżeli jest poprawny to zapisuje dane w bazie danych, jeżeli nie to zwraca komunikat.
     * @param ProjectsRequest $request obiekt klasy pozwalający na validację danych z formularza
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postCreate(ProjectsRequest $request)
    {
        if(!\Auth::user()->checkPrivileges('projects.Create'))
            return view('noAccess');

        $request->validate($request->rules(), $request->messages());

        $entity = new Project();
        $entity->fill($request->all());
        $entity->v_managers = Arr::get(request()->all(), 'managers', []);
        $entity->v_workers = Arr::get(request()->all(), 'workers', []);
        $entity->save();

        $this->addMessage('', __('Pomyślnie dodano projekt'), 'successes');

        return $this->redirect(route('projects.getIndex'));
    }

    /**
     * Zwraca widok formularza edycji projektu
     * @param $id id projektu
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getEdit($id)
    {
        if(!\Auth::user()->checkPrivileges('projects.Edit'))
            return view('noAccess');

        $model = Project::with(['workers', 'workers.worker', 'managers', 'managers.manager'])->find($id);
        $model->fill(request()->old());
        $model->v_managers = Arr::get(request()->all(), 'managers', []);
        $model->v_workers = Arr::get(request()->all(), 'workers', []);

        return view('projects/form', ['entity' => $model]);
    }

    /**
     * Przetwarza przesłany formularz edycji projektów, waliduje go, jeżeli jest poprawny to zapisuje dane w bazie danych, jeżeli nie to zwraca komunikat.
     * @param ProjectsRequest $request obiekt klasy pozwalający na validację danych z formularza
     * @param $id id projektu
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postEdit(ProjectsRequest $request, $id)
    {
        if(!\Auth::user()->checkPrivileges('projects.Edit'))
            return view('noAccess');

        $entity = Project::with(['workers', 'workers.worker', 'managers', 'managers.manager'])->find($id);
        $entity->v_managers = Arr::get(request()->all(), 'managers', []);
        $entity->v_workers = Arr::get(request()->all(), 'workers', []);

        $old = $request->all();

        $entity->fill($old);

        $request->validate($request->rules(), $request->messages());

        $entity->save();

        $this->addMessage('', __('Pomyślnie zaktualizowano dane projektu'), 'successes');

        return $this->redirect(route('projects.getIndex'));
    }

    /**
     * Zwraca widok szczegółów projektu
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getDetails($id)
    {
        if(!\Auth::user()->checkPrivileges('projects.Details'))
            return view('noAccess');

        return view('projects/details', ['entity' => Project::with(['workers', 'workers.worker', 'managers', 'managers.manager'])->find($id)]);
    }

    /**
     * Zwraca widok formularza usuwania projektu
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getDelete($id)
    {
        if(!\Auth::user()->checkPrivileges('projects.Delete'))
            return view('noAccess');

        return view('projects/delete', ['entity' => Project::with(['workers', 'workers.worker', 'managers', 'managers.manager'])->find($id)]);
    }

    /**
     * Przetwarza formularz usuwania projektu oraz usuwa projekt z bazy danych
     * @param Request $request
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postDelete(Request $request, $id)
    {
        if(!\Auth::user()->checkPrivileges('projects.Delete'))
            return view('noAccess');

        Project::find($id)->delete();

        $this->addMessage('', __('Pomyślnie usunięto projekt z bazy danych'), 'successes');

        return $this->redirect(route('projects.getIndex'));
    }

    /**
     * Zwraca kod HTML reprezentujący listę zmian pobraną z systemu zarządzania wersjami Git
     * @param $id
     * @param null $page
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getCommits($id, $page = null)
    {
        if(!\Auth::user()->checkPrivileges('projects.Commits'))
            return view('noAccess');

        $project = Project::find($id);

        $client = new \GuzzleHttp\Client();

        if(!empty($project->git_repository_name))
        {
            $url = preg_replace('/^(https?:\/\/)?(w{0,3}\.)?(.*?)\/(.*?)\/(.*?).git$/', '$1$2$3/api/v4/projects/$4%2F$5', $project->git_repository_name);
            $repository = json_decode($client->get($url)->getBody()->getContents());
        }
        else
        {
            $repository = '';
        }

        $grid = new Grid();
        $grid->getPager()->setPage($page);
        $grid->getPager()->setUrl(route('projects.getCommits'));
        $grid->addCol('title', __('Krótki opis'), 'hidden-xxs');
        $grid->addCol('author_name', __('Autor'), 'hidden-xs');
        $grid->addCol('authored_date', __('Data'), 'hidden-xs');

        if(!isset($repository->message) && !empty($repository))
        {
            $url .= "/repository/commits";
            $grid->getPager()->setTotalResults((int)Arr::first($client->request("GET", $url.'?per_page=1')->getHeader('X-Total')));

            $grid->addAction($repository->web_url . '/commit', 'getCommitDetails', __('Szczegóły commita'), 'fas fa-edit', ['id']);

            $response = $client->request("GET", $url . "?per_page=".$grid->getPager()->getLimit()."&page=".$grid->getPager()->getCurrentPage());
            $commits = json_decode($response->getBody()->getContents());

            $grid->addResults($commits);

            $grid->rowsMap(function($row){

                $row->authored_date = date('Y-m-d H:i:s', strtotime($row->authored_date));

                return $row;
            });
        }

        return view('projects/commits', compact('grid'));
    }

    /**
     * Zwraca listę projektów potrzebną do pola typu Select2
     * @return \Illuminate\Http\JsonResponse
     */
    public function getProjectsList()
    {
        if(!\Auth::user()->checkPrivileges('projects.Create') && !\Auth::user()->checkPrivileges('projects.Edit'))
            return view('noAccess');

        $rows = Project::query()->get();

        $res = [];
        foreach($rows as $row)
        {
            $res[] = [
                'id' => $row->id,
                'text' => $row->name
            ];
        }

        return response()->json($res);
    }

    /**
     * Zwraca widok wykresu Gantta
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getGanttChart($id){
        if(!\Auth::user()->checkPrivileges('projects.GanttChart'))
            return view('noAccess');

        return view('projects/gantt', ['id' => $id]);
    }

    /**
     * Pobiera dane dla wykresu Gantta poprzez zapytanie HTTP
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function getGanntData($id){
        if(!\Auth::user()->checkPrivileges('projects.GanttChart'))
            return view('noAccess');

        //Zadania
        $data = [];

        $tasks = Task::query()->with(['signedTo'])
            ->where('status', "!=", '8')
            ->whereIn('id', function($sql) use ($id){
                $sql->select('task_id')->from('project_tasks')->where('project_id', '=', $id);
            })
            ->orderBy('date_start')->get();
        $tasksRows = [];

        foreach($tasks as $task)
        {
            if($task->signedTo)
            {
                $tasksRows['w' . $task->signedTo->id][] = $task;
            }
        }

        foreach($tasksRows as $key => $tasksRow)
        {
            if(!empty($tasksRow) && $tasksRow[0])
            {
                $duration = 0;
                foreach($tasksRow as $task)
                {
                    $datetime1 = new \DateTime($task->date_start);
                    $datetime2 = new \DateTime($task->date_end);
                    $interval = $datetime1->diff($datetime2);
                    $d = (int)$interval->format('%a');
                    $data[] = [
                        'id' => 't' . $task->id,
                        'text' => $task->name,
                        'start_date' => $task->date_start,
                        'duration' => $d,
                        'parent' => $key,
                        'progress' => 0
                    ];
                    $duration += $d;
                }

                $data[] = [
                    'id' => $key,
                    'text' => $tasksRow[0]->signedTo->getName(),
                    'start_date' => $tasksRow[0]->date_start,
                    'duration' => $duration,
                    'progress' => 0
                ];
            }
        }

        return response()->json([
            'links' => [],
            'data' => $data
        ]);
    }
}
