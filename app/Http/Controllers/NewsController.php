<?php

namespace App\Http\Controllers;

use App\Helpers\Grid;
use App\Helpers\Pager;
use App\Http\Requests\NewsRequest;
use App\Models\Images;
use App\Models\News;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;

/**
 * Klasa odpowiada za obsługę funkcjonalności aktualności
 * @uses  App\Helpers\Grid;
 * @uses  App\Helpers\Pager;
 * @uses  App\Http\Requests\NewsRequest;
 * @uses  App\Models\Images;
 * @uses  App\Models\News;
 * @uses  Illuminate\Http\Request;
 * @uses  Illuminate\Support\Arr;
 * @package App\Http\Controllers
 */
class NewsController extends Controller
{
    /**
     * Wywołanie konstruktora klasy nadrzędnej
     * @see Controller
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Zwraca identyfikator sesji dla tej klasy
     * @return string
     */
    public function getSessionKey()
    {
        return self::class . '__VARIABLES';
    }

    /**
     * Zwraca widok listy aktualności
     * @param null|int $page numer strony
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function getIndex($page = null)
    {
        if(!\Auth::user()->checkPrivileges('news.Index'))
            return view('noAccess');

        $filters = session()->get($this->getSessionKey());
        $grid = new Grid();

        $grid->addCol('name', __('Nazwa'));
        $grid->addCol('created_at', __('Data utworzenia'), 'hidden-sm hidden-xs');
        $grid->addCol('published_at', __('Data publikacji'), 'hidden-xxs');
        $grid->addCol('author', __('Autor'), 'hidden-sm hidden-xs');

        if(\Auth::user()->checkPrivileges('news.Edit'))
            $grid->addAction(route('news.getEdit'), 'editProject', 'Edytuj', 'fas fa-edit', ['id']);

        if(\Auth::user()->checkPrivileges('news.Delete'))
            $grid->addAction(route('news.getDelete'), 'deleteProject', 'Usuń', 'fas fa-trash-alt', ['id']);

        $rowCount = News::query()->where(function($sql) use ($filters){
            if(Arr::has($filters, 'nameFilter'))
            {
                $sql->where('name', 'LIKE', '%'.Arr::get($filters, 'nameFilter').'%');
            }
        })->count();

        $pager = new Pager($rowCount, 10, $page);
        $pager->setUrl(route('news.getIndex'));
        $grid->addPager($pager);

        $rows = News::query()->where(function($sql) use ($filters){
            if(Arr::has($filters, 'nameFilter'))
            {
                $sql->where('name', 'LIKE', '%'.Arr::get($filters, 'nameFilter').'%');
            }
        })->skip($pager->getOffset())->limit($pager->getLimit())->orderBy('created_at', 'desc')->get();

        $grid->addResults($rows);

        $grid->rowsMap(function($row){
            if($row->author)
            {
                $row->author = $row->author->getName();
            }
            else
            {
                $row->author = '';
            }

            return $row;
        });

        return view('news/index', ['grid' => $grid, 'filters' => $filters]);
    }

    /**
     * Zapisuje do sesji filtry oraz przekierowuje na listę aktualności
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function postIndex(Request $request)
    {
        if(!\Auth::user()->checkPrivileges('news.Index'))
            return view('noAccess');

        $variables = $request->only([
            'name', 'published_from', 'published_to'
        ]);

        if(Arr::has($variables, 'remove'))
        {
            unset($variables['name']);
            unset($variables['published_from']);
            unset($variables['published_to']);
        }

        session()->put($this->getSessionKey(), $variables);

        return redirect(route('news.getIndex'));
    }

    /**
     * Zwraca widok formularza dodawania aktualności
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getCreate()
    {
        if(!\Auth::user()->checkPrivileges('news.Create'))
            return view('noAccess');

        $entity = new News(request()->old());
        return view('news/form', ['entity' => $entity]);
    }

    /**
     * Przetwarza przesłany formularz dodawania aktualności, waliduje go, jeżeli jest poprawny to zapisuje dane w bazie danych, jeżeli nie to zwraca komunikat.
     * @param NewsRequest $request obiekt klasy pozwalający na validację danych z formularza
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postCreate(NewsRequest $request)
    {
        if(!\Auth::user()->checkPrivileges('news.Create'))
            return view('noAccess');

        $request->validate($request->rules(), $request->messages());
        $entity = new News();
        $entity->fill($request->all());
        $entity->save();

        $this->addMessage('', __('Pomyślnie dodano akutalność'), 'successes');

        return $this->redirect(route('news.getIndex'));
    }

    /**
     * Zwraca widok formularza edycji aktualności
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getEdit($id)
    {
        if(!\Auth::user()->checkPrivileges('news.Edit'))
            return view('noAccess');

        $entity = News::find($id);
        $entity->fill(request()->old());

        $entity->images = json_decode($entity->images);

        $imgs = [];
        foreach(Images::query()->whereIn('id', $entity->images)->get() as $key => $img)
        {
            $url = public_path('storage/photos/' . $img->url);
            $sizes = getimagesize($url);
            $imgs[$key] = [
                'id' => $img->id,
                'name' => $img->url,
                'type' => $sizes['mime'],
                'url' => "/storage/photos/" . $img->url,
                'height' => $sizes[1],
                'width' => $sizes[0],
                'size' => filesize($url)
            ];
        }
        $entity->images = $imgs;

        return view('news/form', ['entity' => $entity, 'images' => $imgs]);
    }

    /**
     * Przetwarza przesłany formularz edycji aktualności, waliduje go, jeżeli jest poprawny to zapisuje dane w bazie danych, jeżeli nie to zwraca komunikat.
     * @param NewsRequest $request obiekt klasy pozwalający na validację danych z formularza
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postEdit(NewsRequest $request, $id)
    {
        if(!\Auth::user()->checkPrivileges('news.Edit'))
            return view('noAccess');

        $old = $request->all();
        $entity = News::find($id);
        $entity->fill($old);

        $request->validate($request->rules(), $request->messages());

        $entity->save();

        $this->addMessage('', __('Pomyślnie zaktualizowano dane aktualności'), 'successes');

        return $this->redirect(route('news.getIndex'));
    }

    /**
     * Zwraca formularz usuwania aktualności
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getDelete($id)
    {
        if(!\Auth::user()->checkPrivileges('news.Delete'))
            return view('noAccess');

        $entity = News::find($id);
        $entity->images = json_decode($entity->images);

        return view('news/delete', ['entity' => $entity]);
    }

    /**
     * Przetwarza formularz usuwania aktualności oraz usuwa dane z bazy danych
     * @param Request $request
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postDelete(Request $request, $id)
    {
        if(!\Auth::user()->checkPrivileges('news.Delete'))
            return view('noAccess');

        News::find($id)->delete();

        $this->addMessage('', __('Pomyślnie usunięto aktualność z bazy danych'), 'successes');

        return $this->redirect(route('news.getIndex'));
    }
}
