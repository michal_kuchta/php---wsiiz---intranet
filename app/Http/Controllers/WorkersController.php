<?php

namespace App\Http\Controllers;

use App\Helpers\Grid;
use App\Helpers\Pager;
use App\Http\Requests\WorkersRequest;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

/**
 * Klasa odpowiada za obsługę funkcjonalności pracowników
 * @uses  App\Helpers\Grid
 * @uses  App\Helpers\Pager
 * @uses  App\Http\Requests\WorkersRequest
 * @uses  App\Models\User
 * @uses  Illuminate\Http\Request
 * @uses  Illuminate\Support\Facades\Hash
 * @package App\Http\Controllers
 */
class WorkersController extends Controller
{
    /**
     * Zwraca listę do wygenerowania odpowiednich pól do nadawania uprawnień
     * @return array
     */
    public static function getPrivileges()
    {
        return [
            'holidays' => [
                'name' => __('Wnioski urlopowe'),
                'abilities' => [
                    'Index' => __('Lista wniosków'),
                    'Create' => __('Dodawanie wniosków'),
                    'Details' => __('Szczegóły wniosków'),
                ]
            ],
            'projects' => [
                'name' => __('Projekty'),
                'abilities' => [
                    'Index' => __('Lista projektów'),
                    'Create' => __('Dodawanie projektów'),
                    'Edit' => __('Edycja projektów'),
                    'Delete' => __('Usuwanie projektów'),
                    'Details' => __('Szczegóły projektów'),
                    'Commits' => __('Lista zmian z GIT'),
                    'GanttChart' => __('Wykres Gantta'),
                ]
            ],
            'tasks' => [
                'name' => __('Zadania'),
                'abilities' => [
                    'Index' => __('Lista zadań'),
                    'Create' => __('Dodawanie zadań'),
                    'Edit' => __('Edycja zadań'),
                    'Delete' => __('Usuwanie zadań'),
                    'Commits' => __('Lista zmian z GIT'),
                ]
            ],
            'workers' => [
                'name' => __('Pracownicy'),
                'abilities' => [
                    'Index' => __('Lista pracowników'),
                    'Create' => __('Dodawanie pracowników'),
                    'Edit' => __('Edycja pracowników'),
                    'Delete' => __('Usuwanie pracowników'),
                ]
            ],
            'news' => [
                'name' => __('Aktualności'),
                'abilities' => [
                    'Index' => __('Lista aktualności'),
                    'Create' => __('Dodawanie aktualności'),
                    'Edit' => __('Edycja aktualności'),
                    'Delete' => __('Usuwanie aktualności'),
                ]
            ],
            'time' => [
                'name' => __('Czas pracy'),
                'abilities' => [
                    'Index' => __('Lista czasu pracy'),
                    'Create' => __('Dodaj czas pracy'),
                    'Details' => __('Szczegóły czasu pracy'),
                ]
            ],
        ];
    }

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Zwraca widok listy pracowników
     * @param null|int $page numer strony
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function getIndex($page = null)
    {
        if(!\Auth::user()->checkPrivileges('workers.Index'))
            return view('noAccess');

        $grid = new Grid();

        $grid->addCol('name', __('Imię i Nazwisko'));
        $grid->addCol('email', __('E-mail'), 'hidden-xxs');
        $grid->addCol('position', __('Stanowisko'), 'hidden-sm hidden-xs');
        $grid->addCol('activeProjects', __('Ilość projektów'), 'hidden-sm hidden-xs');
        $grid->addCol('isActive', __('Status aktywności'), 'hidden-xs');

        if(\Auth::user()->checkPrivileges('workers.Edit'))
            $grid->addAction(route('workers.getEdit'), 'editWorker', 'Edytuj', 'fas fa-edit', ['id']);

        if(\Auth::user()->checkPrivileges('workers.Delete'))
            $grid->addAction(route('workers.getDelete'), 'deleteWorker', 'Usuń', 'fas fa-trash-alt', ['id']);

        $rowCount = User::query()->count();

        $pager = new Pager($rowCount, 10, $page);
        $pager->setUrl(route('workers.getIndex'));
        $grid->addPager($pager);

        $rows = User::query()->skip($pager->getOffset())->limit($pager->getLimit())->get();

        $grid->addResults($rows);

        $grid->rowsMap(function($row){

            $row->name = $row->first_name . ' ' . $row->last_name;
            $row->isActive = '<span class="'.($row->isActive() ? 'statusActive' : 'statusInActive').'">'.($row->isActive() ? 'Aktywny' : 'Nieaktywny').'</span>';
            $row->activeProjects = $row->projects->count();

            return $row;
        });

        return view('workers/index', ['grid' => $grid]);
    }

    /**
     * Zwraca widok formularza dodawania pracownika
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getCreate()
    {
        if(!\Auth::user()->checkPrivileges('workers.Create'))
            return view('noAccess');

        $model = new User(request()->old());

        return view('workers/form', ['user' => $model, 'privileges' => self::getPrivileges()]);
    }

    /**
     * Przetwarza przesłany formularz dodawania pracowników, waliduje go, jeżeli jest poprawny to zapisuje dane w bazie danych, jeżeli nie to zwraca komunikat.
     * @param WorkersRequest $request obiekt klasy pozwalający na validację danych z formularza
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postCreate(WorkersRequest $request)
    {
        if(!\Auth::user()->checkPrivileges('workers.Create'))
            return view('noAccess');

        $request->validate($request->rules(), $request->messages());

        $entity = new User();
        $entity->fill($request->all());
        $entity->save();

        if($request->has('privileges'))
        {
            foreach($request->post('privileges', []) as $key => $privilege)
            {
                foreach($privilege as $abKey => $ability)
                {
                    if($ability == '1')
                        $entity->allow($key.'.'.$abKey);
                }
            }
        }

        $this->addMessage('', __('Pomyślnie dodano pracownika'), 'successes');

        return $this->redirect(route('workers.getIndex'));
    }

    /**
     * Zwraca widok formularza edycji pracownika
     * @param int $id id pracownika
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getEdit($id)
    {
        if(!\Auth::user()->checkPrivileges('workers.Edit'))
            return view('noAccess');

        $model = User::find($id);
        $model->fill(request()->old());

        return view('workers/form', ['user' => $model, 'privileges' => self::getPrivileges()]);
    }

    /**
     * Przetwarza przesłany formularz edycji pracowników, waliduje go, jeżeli jest poprawny to zapisuje dane w bazie danych, jeżeli nie to zwraca komunikat.
     * @param WorkersRequest $request obiekt klasy pozwalający na validację danych z formularza
     * @param int $id id pracownika
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postEdit(WorkersRequest $request, $id)
    {
        if(!\Auth::user()->checkPrivileges('workers.Edit'))
            return view('noAccess');

        $entity = User::find($id);

        $old = $request->all();

        if(empty($old['password']))
        {
            unset($old['password']);
        }
        else
        {
            $old['password'] = Hash::make($old['password']);
        }

        $entity->fill($old);

        $request->validate($request->rules($entity), $request->messages());

        $entity->save();

        if($request->has('privileges'))
        {
            foreach(self::getPrivileges() as $key => $privilege)
            {
                foreach($privilege as $abKey => $ability)
                {
                    if($ability == '1')
                        $entity->disallow($key.'.'.$abKey);
                }
            }

            foreach($request->post('privileges', []) as $key => $privilege)
            {
                foreach($privilege as $abKey => $ability)
                {
                    if($ability == '1')
                        $entity->allow($key.'.'.$abKey);
                }
            }
        }

        $this->addMessage('', __('Pomyślnie zaktualizowano dane pracownika'), 'successes');

        return $this->redirect(route('workers.getIndex'));
    }

    /**
     * Zwraca widok formularza usuwania pracownika
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getDelete($id)
    {
        if(!\Auth::user()->checkPrivileges('workers.Delete'))
            return view('noAccess');

        return view('workers/delete', ['user' => User::find($id)]);
    }

    /**
     * Przetwarza formularz usuwania pracownika oraz usuwa projekt z bazy danych
     * @param Request $request
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postDelete(Request $request, $id)
    {
        if(!\Auth::user()->checkPrivileges('workers.Delete'))
            return view('noAccess');

        User::find($id)->delete();

        $this->addMessage('', __('Pomyślnie usunięto pracownika z bazy danych'), 'successes');

        return $this->redirect(route('workers.getIndex'));
    }

    /**
     * Zwraca listę pracowników dla pola typu Select2
     * @return \Illuminate\Http\JsonResponse
     */
    public function getUsersList()
    {
        if(!\Auth::user()->checkPrivileges('workers.Create') && !\Auth::user()->checkPrivileges('workers.Edit'))
            return view('noAccess');

        $query = User::query()->where('is_active', '=', '1')->where(function($sql){
            if(request()->has('query') && !empty(request()->post('query')))
            {
                $strs = explode(' ', mb_strtolower(request()->post('query')));

                foreach($strs as $str)
                {
                    $sql->orWhere('first_name', 'like', '%'.$str.'%');
                    $sql->orWhere('last_name', 'like', '%'.$str.'%');
                }
            }
        });

        $res = [];

        foreach($query->get() as $row)
        {
            $res[] = [
                'text' => $row->getName(),
                'id' => $row->id
            ];
        }

        return response()->json($res);
    }
}
