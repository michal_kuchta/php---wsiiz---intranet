<?php

namespace App\Http\Controllers;

use App\Helpers\Grid;
use App\Helpers\Pager;
use App\Http\Requests\TimeRequest;
use App\Models\Project;
use App\Models\Time;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;

/**
 * Klasa odpowiada za obsługę funkcjonalności ewidencji czasu pracy
 * @uses  App\Helpers\Grid
 * @uses  App\Helpers\Pager
 * @uses  App\Http\Requests\TimeRequest
 * @uses  App\Models\Project
 * @uses  App\Models\Time
 * @uses  Carbon\Carbon
 * @uses  Illuminate\Http\Request
 * @uses  Illuminate\Support\Arr
 * @package App\Http\Controllers
 */
class TimeController extends Controller
{
    /**
     * Wywołanie konstruktora klasy nadrzędnej
     * @see Controller
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Zwraca identyfikator sesji dla tej klasy
     * @return string
     */
    public function getSessionKey()
    {
        return self::class . '__VARIABLES';
    }

    /**
     * Zwraca widok listy wykonanych czynności
     * @param null|int $page numer strony
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function getIndex($page = null)
    {
        if(!\Auth::user()->checkPrivileges('time.Index'))
            return view('noAccess');

        $filters = session()->get($this->getSessionKey());

        $grid = new Grid();

        $grid->addCol('project', __('Nazwa projektu'));
        $grid->addCol('task', __('Nazwa zadania'));
        $grid->addCol('date', __('Data'), 'hidden-xxs');
        $grid->addCol('hours', __('Ilość godzin'), 'hidden-sm hidden-xs');

        if(\Auth::user()->checkPrivileges('time.Details'))
            $grid->addAction(route('time.getDetails'), 'timeDetails', 'Szczegóły', 'fas fa-edit', ['id']);

        $rowCount = Time::query()->where(function($sql) use ($filters){
            if(Arr::has($filters, 'date') && !empty($filters['date']))
            {
                $sql->where('date', '=', Carbon::make(Arr::get($filters, 'date'))->toDateTime());
            }
        })->where('user_id', '=', \Auth::id())->count();

        $pager = new Pager($rowCount, 10, $page);
        $pager->setUrl(route('time.getIndex'));
        $grid->addPager($pager);

        $rows = Time::query()->where(function($sql) use ($filters){
            if(Arr::has($filters, 'date') && !empty($filters['date']))
            {
                $sql->where('date', '=', Carbon::make(Arr::get($filters, 'date'))->toDateTime());
            }
        })->where('user_id', '=', \Auth::id())->with(['project','task'])->skip($pager->getOffset())->limit($pager->getLimit())->get();

        $grid->addResults($rows);

        $grid->rowsMap(function($row){
            $row->project = $row->project->name;
            $row->date = Carbon::make($row->date)->format('Y-m-d');
            $row->task = $row->task->name;
            return $row;
        });

        return view('time/index', ['grid' => $grid, 'filters' => $filters]);
    }

    /**
     * Zapisuje do sesji filtry oraz przekierowuje na listę czasu pracy
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function postIndex(Request $request)
    {
        if(!\Auth::user()->checkPrivileges('time.Index'))
            return view('noAccess');

        $variables = $request->only([
            'date'
        ]);

        $variables = array_filter($variables, function($el){
            return !empty($el);
        });

        if(Arr::has($variables, 'remove'))
        {
            unset($variables['date']);
        }

        session()->put($this->getSessionKey(), $variables);

        return redirect(route('time.getIndex'));
    }

    /**
     * Zwraca widok formularza dodawania zadania
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getCreate()
    {
        if(!\Auth::user()->checkPrivileges('time.Create'))
            return view('noAccess');

        $projects = [];
        \Auth::user()->load(['projects', 'projectsWorker']);

        foreach(\Auth::user()->getProjects() as $project)
        {
            $projects[$project->project->id] = $project->project->name;
        }
        $entity = new Time();

        return view('time/form', ['entity' => $entity, 'projects' => $projects]);
    }

    /**
     * Przetwarza przesłany formularz dodawania wykonanych czynności, waliduje go, jeżeli jest poprawny to zapisuje dane w bazie danych, jeżeli nie to zwraca komunikat.
     * @param TimeRequest $request obiekt klasy pozwalający na validację danych z formularza
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postCreate(TimeRequest $request)
    {
        if(!\Auth::user()->checkPrivileges('time.Create'))
            return view('noAccess');

        $request->validate($request->rules(), $request->messages());

        $entity = new Time();
        $entity->fill($request->all());
        $entity->hours = str_replace(',', '.', $entity->hours);
        $entity->user_id = \Auth::id();
        $entity->save();

        $this->addMessage('', __('Pomyślnie dodano czas pracy'), 'successes');

        return $this->redirect(route('time.getIndex'));
    }

    /**
     * Wyświetla szczegóły zapisanych czynności
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getDetails($id)
    {
        $entity = Time::query()->with(['project', 'task'])->findOrFail($id);

        return view('time/details', ['entity' => $entity]);
    }

    /**
     * Zwraca listę zadań przypisaną do wybranego projektu
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getTasks(Request $request)
    {
        $id = $request->get('project');
        $id = intval($id);

        $project = Project::query()->with(['tasks'])->find($id);
        $tasks = [];

        foreach($project->tasks as $task)
        {
            $tasks[] = [
                'id' => $task->task->id,
                'text' => $task->task->name
            ];
        }

        return response()->json($tasks);
    }
}
