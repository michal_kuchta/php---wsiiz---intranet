<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\MessageBag;
use Illuminate\Support\ViewErrorBag;

/**
 * Klasa posiada metody używane przez inne kontrolery
 * @uses Illuminate\Foundation\Bus\DispatchesJobs
 * @uses Illuminate\Routing\Controller
 * @uses Illuminate\Foundation\Validation\ValidatesRequests
 * @uses Illuminate\Foundation\Auth\Access\AuthorizesRequests
 * @uses Illuminate\Support\MessageBag
 * @uses Illuminate\Support\ViewErrorBag
 * @package App\Http\Controllers
 */
class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected $messages; /** @var ViewErrorBag obiekt klasy zawierajacy komunikaty */

    /**
     * Tworzy domyślne rodzaje kontenerów wiadomości
     */
    public function __construct()
    {
        $this->messages = new ViewErrorBag();
        $this->messages->put('errors', new MessageBag());
        $this->messages->put('successes', new MessageBag());
        $this->messages->put('warnings', new MessageBag());
        $this->messages->put('infos', new MessageBag());
    }

    /**
     * Dodaje wiadomość do kontenera
     * @param $key klucz wiadomości
     * @param $message treść wiadomości
     * @param string $bag kontener wiadomości
     */
    protected function addMessage($key, $message, $bag = 'info')
    {
        if(!$this->messages->hasBag($bag))
        {
            $bag = 'info';
        }

        $this->messages->getBag($bag)->add($key, $message);
    }

    /**
     * Przekierowuje na podany adres URL dołączając do sesji kontenery z wiadomościami
     * @param $url adres URL
     * @return \Illuminate\Http\RedirectResponse
     */
    protected function redirect($url)
    {
        $redirect = response()->redirectTo($url);
        $redirect->getSession()->flash('notifications', $this->messages);
        return $redirect;
    }
}
