<?php

namespace App\Http\Controllers;

use App\Models\Project;
use App\Models\User;
use Illuminate\Support\Collection;

/**
 * Odpowiada za obsługę wszystkich funkcjonalności panelu startowego
 * @uses App\Models\Project
 * @uses App\Models\User
 * @uses Illuminate\Support\Collection
 * @package App\Http\Controllers
 */
class DashboardController extends Controller
{
    /**
     * Zwraca widok panelu startowego.
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function getIndex()
    {
        /** @var Collection $lastProjects lista 5 ostatnich projektów */
        $lastProjects = Project::query()->limit(5)->orderBy('created_at')->get();

        /** @var Collection $lastWorkers lista 5 ostatnich pracowników */
        $lastWorkers = User::query()->limit(5)->orderBy('created_at')->get();

        /** @var Collection $myProjects lista 5 moich projektów */
        $myProjects = \Auth::user()->getProjects();

        /** @var Collection $myTasks lista 5 moich zadań */
        $myTasks = \Auth::user()->getTasks();

        return view('dashboard/index', compact('lastProjects', 'lastWorkers', 'myProjects', 'myTasks'));
    }
}
