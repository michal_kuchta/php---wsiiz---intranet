<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;

/**
 * Klasa odpowiada za autoryzowanie użytkowników
 * @uses App\Http\Controllers\Controller
 * @uses Illuminate\Foundation\Auth\AuthenticatesUsers
 * @uses Illuminate\Http\Request
 * @package App\Http\Controllers\Auth
 */
class LoginController extends Controller
{
    use AuthenticatesUsers{
        logout as performLogout;
    }

    protected $redirectTo = '/dashboard'; /** @var string Oznacza adres na który użytkownik ma zostać przekierowany po zalogowaniu */
    protected $username = 'username'; /** @var string Oznacza przy użyciu jakiego pola w bazie użytkownik ma być autoryzowany */

    /**
     * Tworzy nową instancję klasy
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    /**
     * Oznacza przy użyciu jakiego pola w bazie użytkownik ma być autoryzowany
     * @return string
     */
    public function username()
    {
        return 'username';
    }

    /**
     * Wylogowuje użytkownika z serwisu
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function logout (Request $request)
    {
        $this->performLogout($request);
        return redirect()->route('login');
    }
}
