<?php

namespace App\Http\Controllers;

use App\Helpers\Grid;
use App\Helpers\Pager;
use App\Http\Requests\TasksRequest;
use App\Models\Project;
use App\Models\ProjectManager;
use App\Models\ProjectTask;
use App\Models\ProjectWorker;
use App\Models\Task;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;

/**
 * Klasa odpowiada za obsługę funkcjonalności zadań
 * @uses  App\Helpers\Grid
 * @uses  App\Helpers\Pager
 * @uses  App\Http\Requests\TasksRequest
 * @uses  App\Models\Project
 * @uses  App\Models\ProjectManager
 * @uses  App\Models\ProjectTask
 * @uses  App\Models\ProjectWorker
 * @uses  App\Models\Task
 * @uses  Illuminate\Http\Request
 * @uses  Illuminate\Support\Arr
 * @package App\Http\Controllers
 */
class TasksController extends Controller
{
    /**
     * Wywołanie konstruktora klasy nadrzędnej
     * @see Controller
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Zwraca identyfikator sesji dla tej klasy
     * @return string
     */
    public function getSessionKey()
    {
        return self::class . '__VARIABLES';
    }

    /**
     * Zwraca widok listy zadań
     * @param null|int $page numer strony
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function getIndex($page = null)
    {
        if(!\Auth::user()->checkPrivileges('tasks.Index'))
            return view('noAccess');

        $filters = session()->get($this->getSessionKey());
        $grid = new Grid();

        $grid->addCol('name', __('Nazwa'));
        $grid->addCol('created_at', __('Data utworzenia'), 'hidden-xxs');
        $grid->addCol('signed_to', __('Przypisane do'), 'hidden-sm hidden-xs');
        $grid->addCol('status', __('Status'), 'hidden-xs');

        if(\Auth::user()->checkPrivileges('tasks.Edit')) {
            $grid->addCol('hours', __('Ilość godzin'), 'hidden-xs');
            $grid->addAction(route('tasks.getEdit'), 'editProject', 'Edytuj', 'fas fa-edit', ['id']);
        }

        if(\Auth::user()->checkPrivileges('tasks.Delete'))
            $grid->addAction(route('tasks.getDelete'), 'deleteProject', 'Usuń', 'fas fa-trash-alt', ['id']);

        $rowCount = ProjectTask::query()->count();

        $pager = new Pager($rowCount, 10, $page);
        $pager->setUrl(route('tasks.getIndex'));
        $grid->addPager($pager);

        $rows = ProjectTask::query()->with(['task'])->where(function($sql) use ($filters){
            if(Arr::has($filters, 'selectProject'))
            {
                $sql->where('project_id', '=', Arr::get($filters, 'selectProject'));
            }
            else
            {
                $sql->whereIn('project_id', function($sql) use ($filters){
                    $sql->from('project_workers')->select('project_id')->where('user_id', '=', \Auth::id())->pluck('project_id');
                });
            }
        })->skip($pager->getOffset())->limit($pager->getLimit())->get();

        $grid->addResults($rows);

        $grid->rowsMap(function($row){
            $row = $row->task;
            if($row->signedTo)
            {
                $row->signed_to = $row->signedTo->getName();
            }
            else
            {
                $row->signed_to = '';
            }

            if(\Auth::user()->checkPrivileges('tasks.Edit')) {
                if($row->hours > 0)
                {
                    $row->hours = $row->getTotalHours() . ' / ' . $row->hours;
                }
                else
                {
                    $row->hours = $row->getTotalHours();
                }
            }

            $row->status = Arr::get($row->getStatuses(), $row->status, '');
            return $row;
        });

        $projects = [];
        foreach(ProjectManager::query()->with('project')->where('user_id', '=', \Auth::id())->get() as $project)
        {
            $projects[$project->project->id] = $project->project->name;
        }

        foreach(ProjectWorker::query()->with('project')->where('user_id', '=', \Auth::id())->get() as $project)
        {
            $projects[$project->project->id] = $project->project->name;
        }

        return view('tasks/index', ['grid' => $grid, 'filters' => $filters, 'projects' => $projects]);
    }

    /**
     * Zapisuje do sesji filtry oraz przekierowuje na listę zadań
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function postIndex(Request $request)
    {
        if(!\Auth::user()->checkPrivileges('tasks.Index'))
            return view('noAccess');

        $variables = $request->only([
            'selectProject'
        ]);

        if(Arr::get($variables, 'selectProject', 0) == '__DELETE__DEFAULT__OPTION__')
        {
            unset($variables['selectProject']);
        }

        session()->put($this->getSessionKey(), $variables);

        return redirect(route('tasks.getIndex'));
    }

    /**
     * Zwraca widok formularza dodawania zadania
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getCreate()
    {
        if(!\Auth::user()->checkPrivileges('tasks.Create'))
            return view('noAccess');

        $entity = new Task(request()->old());
        $entity->v_projectId = Arr::get(request()->old(), 'project_id', 0);
        $entity->v_signedTo = Arr::get(request()->old(), 'signed_to', 0);
        return view('tasks/form', ['entity' => $entity]);
    }

    /**
     * Przetwarza przesłany formularz dodawania zadań, waliduje go, jeżeli jest poprawny to zapisuje dane w bazie danych, jeżeli nie to zwraca komunikat.
     * @param TasksRequest $request obiekt klasy pozwalający na validację danych z formularza
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postCreate(TasksRequest $request)
    {
        if(!\Auth::user()->checkPrivileges('tasks.Create'))
            return view('noAccess');

        $request->validate($request->rules(), $request->messages());
        $entity = new Task();
        $entity->fill($request->all());
        $entity->v_projectId = $request->get('project_id');
        $entity->save();

        $this->addMessage('', __('Pomyślnie dodano zadanie'), 'successes');

        return $this->redirect(route('tasks.getIndex'));
    }

    /**
     * Zwraca widok formularza edycji zadania
     * @param $id id zadania
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getEdit($id)
    {
        if(!\Auth::user()->checkPrivileges('tasks.Edit'))
            return view('noAccess');

        $entity = Task::with(['signedTo', 'project'])->find($id);
        $entity->fill(request()->old());
        $entity->v_projectId = Arr::get(request()->old(), 'project_id', 0);
        $entity->v_signedTo = Arr::get(request()->old(), 'signed_to', 0);
        return view('tasks/form', ['entity' => $entity]);
    }

    /**
     * Przetwarza przesłany formularz edycji zadań, waliduje go, jeżeli jest poprawny to zapisuje dane w bazie danych, jeżeli nie to zwraca komunikat.
     * @param TasksRequest $request obiekt klasy pozwalający na validację danych z formularza
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postEdit(TasksRequest $request, $id)
    {
        if(!\Auth::user()->checkPrivileges('tasks.Edit'))
            return view('noAccess');

        $old = $request->all();
        $entity = Task::with(['signedTo', 'project'])->find($id);
        $entity->v_projectId = Arr::get($old, 'project_id', 0);
        $entity->v_signedTo = Arr::get($old, 'signed_to', 0);

        $entity->fill($old);

        $request->validate($request->rules(), $request->messages());

        $entity->save();

        $this->addMessage('', __('Pomyślnie zaktualizowano dane zadania'), 'successes');

        return $this->redirect(route('tasks.getIndex'));
    }

    /**
     * Zwraca formularz usuwania zadania
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getDelete($id)
    {
        if(!\Auth::user()->checkPrivileges('tasks.Delete'))
            return view('noAccess');

        return view('tasks/delete', ['entity' => Task::with(['signedTo', 'project'])->find($id)]);
    }

    /**
     * Przetwarza formularz usuwania zadania oraz usuwa dane z bazy danych
     * @param Request $request
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postDelete(Request $request, $id)
    {
        if(!\Auth::user()->checkPrivileges('tasks.Delete'))
            return view('noAccess');

        Task::find($id)->delete();

        $this->addMessage('', __('Pomyślnie usunięto zadanie z bazy danych'), 'successes');

        return $this->redirect(route('tasks.getIndex'));
    }

    /**
     * Zwraca kod HTML reprezentujący listę zmian pobraną z systemu zarządzania wersjami Git
     * @param $id
     * @param null $page
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getCommits($id, $page = null)
    {
        if(!\Auth::user()->checkPrivileges('tasks.Commits'))
            return view('noAccess');

        $project = Project::find($id);

        $client = new \GuzzleHttp\Client();

        if(!empty($project->git_repository_name))
        {
            $url = preg_replace('/^(https?:\/\/)?(w{0,3}\.)?(.*?)\/(.*?)\/(.*?).git$/', '$1$2$3/api/v4/projects/$4%2F$5', $project->git_repository_name);
            $repository = json_decode($client->get($url)->getBody()->getContents());
        }
        else
        {
            $repository = '';
        }

        $grid = new Grid();
        $grid->getPager()->setPage($page);
        $grid->getPager()->setUrl(route('projects.getCommits'));
        $grid->addCol('title', __('Krótki opis'), 'hidden-xxs');
        $grid->addCol('author_name', __('Autor'), 'hidden-xs');
        $grid->addCol('authored_date', __('Data'), 'hidden-xs');

        if(!isset($repository->message) && !empty($repository))
        {
            $url .= "/repository/commits";
            $grid->getPager()->setTotalResults((int)Arr::first($client->request("GET", $url.'?per_page=1')->getHeader('X-Total')));

            $grid->addAction($repository->web_url . '/commit', 'getCommitDetails', __('Szczegóły commita'), 'fas fa-edit', ['id']);

            $response = $client->request("GET", $url . "?per_page=".$grid->getPager()->getLimit()."&page=".$grid->getPager()->getCurrentPage());
            $commits = json_decode($response->getBody()->getContents());

            $grid->addResults($commits);

            $grid->rowsMap(function($row){

                $row->authored_date = date('Y-m-d H:i:s', strtotime($row->authored_date));

                return $row;
            });
        }

        return view('projects/commits', compact('grid'));
    }
}
