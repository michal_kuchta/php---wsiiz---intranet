<?php

namespace App\Http\Controllers;

use App\Models\Images;
use Illuminate\Http\Request;

/**
 * Klasa odpowiada za zapis zdjęć na serwerze
 * @uses App\Models\Images
 * @uses Illuminate\Http\Request
 * @package App\Http\Controllers
 */
class UploadController extends Controller
{

    /**
     * Zapisuje zdjęcia w bazie danych
     * @param Request $request
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function save(Request $request)
    {
        if(!\Auth::user()->checkPrivileges('news.Edit') || !\Auth::user()->checkPrivileges('news.Create'))
            return view('noAccess');

        $img = $request->file('file');
        $imageName = $img->getClientOriginalName();

        $imageName = preg_replace("/(.*)\.(.*)$/", '$1', $imageName);

        $count = Images::query()->where('url', '=', $imageName)->count();
        if($count > 0)
        {
            $imageName .= "(".($count+1).")";
        }

        $imageNameFull = $imageName . '.' . $img->getClientOriginalExtension();
        $img->move(public_path('storage/photos'),$imageNameFull);

        $sizes = getimagesize(public_path('storage/photos/' . $imageNameFull));
        $image = new Images(['url' => $imageNameFull]);
        $image->save();
        return response()->json(['id' => $image->id, 'image' => $imageNameFull, 'width' => $sizes[0], 'height' => $sizes[1]]);
    }
}
