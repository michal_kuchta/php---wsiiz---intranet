<?php

namespace App\Http\Controllers;

use App\Helpers\Grid;
use App\Helpers\Pager;
use App\Http\Requests\HolidayRequestsRequest;
use App\Models\HolidayRequest;
use App\Models\User;
use Illuminate\Http\Request;

/**
 * Klasa odpowiada za obsługę funkcjonalności wniosków urlopowych
 * @uses  App\Helpers\Grid
 * @uses  App\Helpers\Pager
 * @uses  App\Http\Requests\HolidayRequestsRequest
 * @uses  App\Models\HolidayRequest
 * @uses  App\Models\User
 * @uses  Illuminate\Http\Request
 * @package App\Http\Controllers
 */
class HolidaysController extends Controller
{
    /**
     * Wywołanie konstruktora klasy nadrzędnej
     * @see Controller
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Zwraca identyfikator sesji dla tej klasy
     * @return string
     */
    public function getSessionKey()
    {
        return self::class . '__VARIABLES';
    }

    /**
     * Zwraca widok listy wniosków urlopowych
     * @param null|int $page numer strony
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function getIndex($page = null)
    {
        if(!\Auth::user()->checkPrivileges('holidays.Index'))
            return view('noAccess');

        $grid = new Grid();

        $grid->addCol('date_from', __('Data od'));
        $grid->addCol('date_to', __('Data do'), 'hidden-xxs');
        $grid->addCol('name', __('Imię i nazwisko'), 'hidden-sm hidden-xs');
        $grid->addCol('status', __('Status'), 'hidden-xs');
        $grid->addCol('type', __('Typ'), 'hidden-xs');
        $grid->addCol('duration', __('Czas trwania'), 'hidden-xs');

        if(\Auth::user()->checkPrivileges('holidays.Details'))
            $grid->addAction(route('holidays.getDetails'), 'editProject', 'Szczegóły', 'fas fa-edit', ['id']);

        $userIds = [\Auth::id()];
        if (\Auth::user()->isAdmin()) {
            $userIds = array_merge($userIds, User::query()->select(['id'])->get()->pluck('id')->toArray());
        }

        $rowCount = HolidayRequest::query()->whereIn('user_id', $userIds)->count();

        $pager = new Pager($rowCount, 10, $page);
        $pager->setUrl(route('holidays.getIndex'));
        $grid->addPager($pager);

        $rows = HolidayRequest::query()->with(['user'])->whereIn('user_id', $userIds)->skip($pager->getOffset())->limit($pager->getLimit())->get();

        $grid->addResults($rows);

        $grid->rowsMap(function ($row) {
            $row->name = $row->user->getName();
            $row->status = $row->getStatus($row->status);
            $row->type = $row->getType($row->type);

            $row->date_from = date('Y-m-d', strtotime($row->date_from));
            $row->date_to = date('Y-m-d', strtotime($row->date_to));

            $datetime1 = new \DateTime($row->date_from);
            $datetime2 = new \DateTime($row->date_to);
            $interval = $datetime1->diff($datetime2);
            $row->duration = $interval->format('%a') . ' ' .__('dni');

            return $row;
        });

        return view('holidays/index', ['grid' => $grid]);
    }

    /**
     * Zwraca widok formularza dodawania wniosku urlopowego
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getCreate()
    {
        if(!\Auth::user()->checkPrivileges('holidays.Create'))
            return view('noAccess');

        $entity = new HolidayRequest(request()->old());
        return view('holidays/form', ['entity' => $entity]);
    }

    /**
     * Przetwarza przesłany formularz dodawania wniosków urlopowych, waliduje go, jeżeli jest poprawny to zapisuje dane w bazie danych, jeżeli nie to zwraca komunikat.
     * @param HolidayRequestsRequest $request obiekt klasy pozwalający na validację danych z formularza
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postCreate(HolidayRequestsRequest $request)
    {
        if(!\Auth::user()->checkPrivileges('holidays.Create'))
            return view('noAccess');

        $request->validate($request->rules(), $request->messages());
        $entity = new HolidayRequest();
        $entity->fill($request->all());
        $entity->user_id = \Auth::id();
        $entity->save();

        $this->addMessage('', __('Pomyślnie dodano wniosek'), 'successes');

        return $this->redirect(route('holidays.getIndex'));
    }

    /**
     * Zwraca widok listy szczegółów wniosku
     * @param int $id identyfikator wniosku
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getDetails($id)
    {
        if(!\Auth::user()->checkPrivileges('holidays.Details'))
            return view('noAccess');

        return view('holidays/details', ['entity' => HolidayRequest::with(['user'])->find($id)]);
    }

    /**
     * Przetwarza przesłąny formularz szczegółów wniosków urlopowych, waliduje go, jeżeli jest poprawny to zapisuje dane w bazie danych, jeżeli nie to zwraca komunikat.
     * @param Request $request
     * @param int $id identyfikator wniosku
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postDetails(Request $request, $id)
    {
        if(!\Auth::user()->checkPrivileges('holidays.Details'))
            return view('noAccess');

        $entity = HolidayRequest::find($id);
        $entity->status = $request->post('status', '1');
        $entity->save();

        $this->addMessage('', __('Pomyślnie zaktualizowano status wniosku'), 'successes');

        return $this->redirect(route('holidays.getIndex'));
    }
}
