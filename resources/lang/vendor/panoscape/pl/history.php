<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Tracker Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used across application for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'created' => 'Utworzenie nowego wpisu: :label',

    'updating' => 'Aktualizacja wpisu: :label',

    'deleting' => 'Usunięcie wpisu: :label',

    'restored' => 'Przywrócenie wpisu :label',

    //you may added your own model name language line here
    'models' => [

    ]
];
