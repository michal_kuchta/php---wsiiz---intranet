<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        @include('partials/head')
    </head>
    <body>
        <section class="content row">
            @include('partials/topBar')
            @include('partials/mainMenu')
            <section class="page" style="height: 100%">
                @include('partials/messages')
                @yield('content')
            </section>
        </section>
    </body>
</html>
