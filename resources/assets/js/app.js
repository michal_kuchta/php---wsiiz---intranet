
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

function htmlbodyHeightUpdate(){
    var height3 = $( window ).height()
    var height1 = $('.nav').height()+50
    height2 = $('.main').height()
    if(height2 > height3){
        $('html').height(Math.max(height1,height3,height2)+10);
        $('body').height(Math.max(height1,height3,height2)+10);
    }
    else
    {
        $('html').height(Math.max(height1,height3,height2));
        $('body').height(Math.max(height1,height3,height2));
    }

}
function htmlBodyOnResizeFunc()
{
    var width = $(window).width();
    if(width > 768)
    {
        $('.navbar-toggle').removeClass('collapsed');
    }
}

function checboxPrepare()
{
    $(".checkbox > var, .checkbox > label").on("click", function(){
        var input = $(this).parent().find('input[type="checkbox"]');
        if(input.is(":checked"))
        {
            input.removeAttr('checked');
        }
        else
        {
            input.attr('checked', 'checked');
        }
    });
}
$(document).ready(function () {
    htmlbodyHeightUpdate();
    htmlBodyOnResizeFunc();
    checboxPrepare();
    $( window ).resize(function() {
        htmlbodyHeightUpdate();
        htmlBodyOnResizeFunc();
    });
    $( window ).scroll(function() {
        height2 = $('.main').height()
        htmlbodyHeightUpdate();
        htmlBodyOnResizeFunc();
    });
});
