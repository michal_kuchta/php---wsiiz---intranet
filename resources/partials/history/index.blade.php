@if(!isset($histories) || empty($histories))
    <h2>{{ __('Brak zmian w historii') }}</h2>
@else
    @foreach($histories as $history)
        <div class="row pl-3">
            <div class="col-6 pb-4 pt-4 mt-4 mb-4 border" style="background: #e6e6e6; -webkit-border-radius: 10px;-moz-border-radius: 10px;border-radius: 10px; border-color: #cecece;">
                <div class="row">
                    <div class="col-2 text-right">{{ __('Operacja') }}:</div>
                    <div class="col-6">{!! $history->message !!}</div>
                </div>
                <div class="row">
                    <div class="col-2 text-right">{{ __('Data') }}:</div>
                    <div class="col-6">{!! $history->performed_at !!}</div>
                </div>
                @if($history->meta)
                    <div class="row">
                        <div class="col-2 text-right">{{ __('Zmiany') }}:</div>
                        <div class="col">
                            <table style='width:100%'>
                                <thead>
                                    <tr>
                                        <th>{{ __('Nazwa pola') }}</th>
                                        <th>{{ __('Przed') }}</th>
                                        <th>{{ __('Po') }}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($history->meta as $r)
                                        <tr>
                                            <td>{!! $history->model()->getFieldName($r['key']) !!}</td>
                                            <td>{!! $r['old'] !!}</td>
                                            <td>{!! $r['new'] !!}</td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                @endif
            </div>
        </div>
    @endforeach
@endif
