@foreach($notifications->getBags() as $key => $bag)
    @if($bag->count() > 0)
        @switch($key)
            @case('errors')
            @case('error')
                <div class="alert alert-danger alert-dismissible">
            @break
            @case('warnings')
            @case('warning')
                <div class="alert alert-warning alert-dismissible">
            @break
            @case('successes')
            @case('success')
                <div class="alert alert-success alert-dismissible">
            @break
            @case('infos')
            @case('info')
                <div class="alert alert-info alert-dismissible">
            @break
        @endswitch
            <ul class="pt-3 pb-3">
                @foreach($notifications->getBag($key)->all() as $name => $message)
                    <li>{{ $message }}</li>
                @endforeach
            </ul>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    @endif
@endforeach