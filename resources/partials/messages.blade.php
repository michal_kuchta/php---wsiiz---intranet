@include('partials/messagesBlock', ['notifications' => $errors])
@isset($notifications)
    @include('partials/messagesBlock', ['notifications' => $notifications])
@endisset
@if(session()->has('notifications'))
    @include('partials/messagesBlock', ['notifications' => session()->get('notifications')])
@endif