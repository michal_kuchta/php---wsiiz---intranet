<nav class="navbar navbar-inverse sidebar" role="navigation">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-sidebar-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="{{ route('home') }}"><img id="logo2" src="{{ url('/images/logo2.svg') }}" /><img id="logo3" src="{{ url('/images/logo3.svg') }}" /></a>
        </div>
        <div class="collapse navbar-collapse collapsable" id="bs-sidebar-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li><a href="{{ route('home') }}" @if(\Request::route()->uri === route('home')) class="active"@endif>{{ __('Strona główna') }}<span style="font-size:16px;" class="pull-right hidden-xs showopacity fas fa-home"></span></a></li>
                @if(\Auth::user()->checkPrivileges('workers.Index'))<li><a href="{{ url('/workers') }}" @if(\Request::route()->uri === route('workers.getIndex')) class="active"@endif>{{ __('Pracownicy') }}<span style="font-size:16px;" class="pull-right hidden-xs showopacity far fa-user"></span></a></li>@endif
                @if(\Auth::user()->checkPrivileges('projects.Index'))<li><a href="{{ url('/projects') }}" @if(\Request::route()->uri === route('projects.getIndex')) class="active"@endif>{{ __('Projekty') }}<span style="font-size:16px;" class="pull-right hidden-xs showopacity fas fa-clipboard-list"></span></a></li>@endif
                @if(\Auth::user()->checkPrivileges('tasks.Index'))<li><a href="{{ url('/tasks') }}" @if(\Request::route()->uri === route('tasks.getIndex')) class="active"@endif>{{ __('Zadania') }}<span style="font-size:16px;" class="pull-right hidden-xs showopacity fas fa-book"></span></a></li>@endif
                @if(\Auth::user()->checkPrivileges('holidays.Index'))<li><a href="{{ url('/holidays') }}" @if(\Request::route()->uri === route('holidays.getIndex')) class="active"@endif>{{ __('Urlopy') }}<span style="font-size:16px;" class="pull-right hidden-xs showopacity fas fa-file-invoice"></span></a></li>@endif
                @if(\Auth::user()->checkPrivileges('news.Index'))<li><a href="{{ url('/news') }}" @if(\Request::route()->uri === route('news.getIndex')) class="active"@endif>{{ __('Aktualnosci') }}<span style="font-size:16px;" class="pull-right hidden-xs showopacity fas fa-newspaper"></span></a></li>@endif
                @if(\Auth::user()->checkPrivileges('time.Index'))<li><a href="{{ url('/time') }}" @if(\Request::route()->uri === route('time.getIndex')) class="active"@endif>{{ __('Czas pracy') }}<span style="font-size:16px;" class="pull-right hidden-xs showopacity fas fa-calendar-alt"></span></a></li>@endif
            </ul>
        </div>
    </div>
</nav>
