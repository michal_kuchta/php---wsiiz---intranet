<section class="grid-view">
    <div class="row grid-header">
        @foreach($cols as $col)
            <div class="col {!! $col->get('class') !!}">{!! $col->get('text') !!}</div>
        @endforeach
        @if(!$rowActions->isEmpty())
            <div class="col-2 actions"><span class="hidden-xs">Akcja</span></div>
        @endif
    </div>
    @if($rows->count() == 0)
        <div class="row grid-row align-content-center">
            <div style="text-align: center;padding: 10px;width: 100%">
                <h5 style="margin:0;">{{ __('Brak danych') }}</h5>
            </div>
        </div>
    @endif
    @foreach($rows as $row)
        <div class="row grid-row">
            @foreach($cols as $col)
                @if(!empty($col->get('name')))
                    @if(is_array($row))
                        <div class="col {!! $col->get('class') !!}">{!! $row[$col->get('name')] !!}</div>
                    @else
                        <div class="col {!! $col->get('class') !!}">{!! $row->{$col->get('name')} !!}</div>
                    @endif
                @else
                    <div class="col {!! $col->get('class') !!}"></div>
                @endif
            @endforeach
            @if(!$rowActions->isEmpty())
                <div class="col-2 actions">
                    <div class="gridActionNavbar">
                        @foreach($rowActions as $rowAction)
                            @php
                                $url = $rowAction->get('url');

                                if(count($rowAction->get('params')) > 0)
                                {
                                    $parts = [];
                                    foreach($rowAction->get('params') as $param)
                                    {
                                        if(!empty($param))
                                        {
                                            if(is_array($row))
                                            {
                                                $parts[] = $row[$param];
                                            }
                                            else
                                            {
                                                $parts[] = $row->$param;
                                            }
                                        }
                                    }

                                    if(!empty($parts))
                                    {
                                        $url .= '/' . implode('/', $parts);
                                    }
                                }
                            @endphp
                            <a href="{!! $url !!}" title="{!! $rowAction->get('text') !!}"><i class="{!! $rowAction->get('icon') !!}"></i></a>
                            <span class="spacer"></span>
                        @endforeach
                    </div>
                </div>
            @endif
        </div>
    @endforeach
    <div class="row grid-footer"></div>
    {!! $pager !!}
</section>