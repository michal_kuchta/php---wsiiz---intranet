<header class="topBar">
    <form action="{{ url('/logout') }}" method="POST">
        @csrf
        <button type="submit">
            <span style="font-size:16px;" class="hidden-xs showopacity fas fa-sign-out-alt"></span>
            {{ __('Wyloguj') }}
        </button>
    </form>
</header>