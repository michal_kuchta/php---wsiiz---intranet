@extends('layouts.app')

@section('content')
    <header>
        <h2>Lista zmian w GIT</h2>
    </header>
    {!! $grid !!}
    <br>
    {!! Form::group(Form::buttons([
        [
            'type' => 'link',
            'class' => 'btn btn-danger',
            'value' => __('Powrót'),
            'href' => route('projects.getIndex')
        ]
    ])) !!}
@endsection
