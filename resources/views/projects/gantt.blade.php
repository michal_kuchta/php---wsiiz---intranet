@extends('layouts.app')

@section('content')
    <header>
        <h2>Harmonogram zadań - wykres Gantta</h2>
    </header>

    <div id="ganttChart" style='width:100%; height:100%;'></div>
    <script type="text/javascript">
        gantt.config.xml_date = "%Y-%m-%d %H:%i:%s";
        gantt.init("ganttChart");
        gantt.load("/api/projects/ganttData/{{ $id }}");
    </script>
@endsection
