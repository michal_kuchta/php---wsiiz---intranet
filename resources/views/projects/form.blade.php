@extends('layouts.app')

@section('content')
    <header>
        @if(\Request::route()->getName() == 'projects.getEdit')
            <h2>{{ __('Edycja projektu') }}</h2>
        @else
            <h1>{{ __('Dodanie nowego projektu') }}</h1>
        @endif
    </header>


    {!! Form::open('', 'POST') !!}

        {!! Form::addTabs([__('Szczegóły'), __('Historia')]) !!}

        {!! Form::addTab(__('Szczegóły')) !!}

        {!! Form::group(Form::input('text', 'name', __('Nazwa'), $entity ? $entity->name : '', true)) !!}
        {!! Form::group(Form::input('textarea', 'description', __('Opis projektu'), $entity ? $entity->description : '', true)) !!}
        {!! Form::group(Form::input('text', 'git_repository_name', __('Ścieżka projektu GIT'), $entity ? $entity->git_repository_name : '', false)) !!}
        {!! Form::group(Form::select2('managers', __('Kierownicy projektu'), $entity ? $entity->managersList() : [], $entity ? array_keys($entity->managersList()) : [], route('api.workers.getUsersList'), true)) !!}
        {!! Form::group(Form::select2('workers', __('Pracownicy'), $entity ? $entity->workersList() : [], $entity ? array_keys($entity->workersList()) : [], route('api.workers.getUsersList'))) !!}
        {!! Form::group(__('Ilość przepracowanych godzin: ') . $entity->getTotalHours()) !!}
        {!! Form::group(Form::input('number', 'budget', __('Budżet'), $entity ? $entity->budget : '')) !!}
        {!! Form::group(Form::input('number', 'hourly_rate', __('Stawka godzinowa'), $entity ? $entity->hourly_rate : '')) !!}
        {!! Form::checkbox('is_active', __('Projekt aktywny'), $entity && $entity->isActive(), false) !!}
        {!! Form::checkbox('is_public', __('Projekt publiczny'), $entity && $entity->isPublic(), false) !!}

        {!! Form::addTab(__('Historia')) !!}

        {!! \App\Helpers\History::render($entity) !!}

        {!! Form::closeTab() !!}

        {!! Form::group(Form::buttons([
            [
                'type' => 'submit',
                'class' => 'btn btn-success',
                'value' => __('Zapisz'),
            ],[
                'type' => 'link',
                'class' => 'btn btn-danger',
                'value' => __('Powrót'),
                'href' => route('projects.getIndex')
            ]
        ])) !!}
    {!! Form::close() !!}

@endsection
