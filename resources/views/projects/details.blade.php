@extends('layouts.app')

@section('content')
    <header>
        <h2>{{ __('Szczegóły projektu') }}</h2>
    </header>
    <br>
    <br>
    {!! Form::open('', 'POST') !!}
    <form method="post">
        {!! Form::group(Form::row(__('Nazwa'), $entity->name)) !!}
        {!! Form::group(Form::row(__('Opis projektu'), $entity->description)) !!}
        {!! Form::group(Form::row(__('Ścieżka projektu GIT'), $entity->git_repository_name)) !!}
        @foreach($entity->managers as $key => $manager)
            @if($key == 0)
                {!! Form::group(Form::row(__('Kierownicy projektu'), $manager->manager->getName())) !!}
            @else
                {!! Form::group(Form::row('', $manager->manager->getName())) !!}
            @endif
        @endforeach
        @foreach($entity->workers as $key => $worker)
            @if($key == 0)
                {!! Form::group(Form::row(__('Pracownicy'), $worker->worker->getName())) !!}
            @else
                {!! Form::group(Form::row('', $worker->worker->getName())) !!}
            @endif
        @endforeach

        <br>
        <br>
        {!! Form::group(Form::buttons([
            [
                'type' => 'link',
                'class' => 'btn btn-success',
                'value' => __('Powrót'),
                'href' => route('home')
            ]
        ])) !!}
    {!! Form::close() !!}

@endsection
