@extends('layouts.app')

@section('content')
    <header>
        <h2>{{ __('Lista projektów') }}</h2>
        @if(\Auth::user()->checkPrivileges('projects.Create'))<a href="{{ route('projects.getCreate') }}" class="btn btn-success bottom-action-button"><i class="fas fa-plus"></i> <span>Dodaj projekt</span></a>@endif
    </header>
    {!! $grid !!}
@endsection
