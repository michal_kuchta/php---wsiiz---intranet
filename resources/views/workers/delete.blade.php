@extends('layouts.app')

@section('content')
    <header>
        <h1>{{ __('Czy jesteś pewien, że chcesz usunąć tego pracownika?') }}</h1>
    </header>
    <br>
    <br>
    {!! Form::open('', 'POST') !!}
    <form method="post">
        {!! Form::group(Form::row(__('Imię'), $user->first_name)) !!}
        {!! Form::group(Form::row(__('Nazwisko'), $user->last_name)) !!}
        {!! Form::group(Form::row(__('Login'), $user->username)) !!}
        {!! Form::group(Form::row(__('Stanowisko'), $user->position)) !!}
        {!! Form::group(Form::row(__('Adres e-mail'), $user->email)) !!}

        <br>
        <br>
        {!! Form::group(Form::buttons([
            [
                'type' => 'submit',
                'class' => 'btn btn-danger',
                'value' => __('Usuń'),
            ],[
                'type' => 'link',
                'class' => 'btn btn-success',
                'value' => __('Powrót'),
                'href' => route('workers.getIndex')
            ]
        ])) !!}
    {!! Form::close() !!}

@endsection
