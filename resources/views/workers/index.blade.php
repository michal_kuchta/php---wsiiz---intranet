@extends('layouts.app')

@section('content')
    <header>
        <h2>{{ __('Lista pracowników') }}</h2>
        @if(\Auth::user()->checkPrivileges('workers.Create'))<a href="{{ route('workers.getCreate') }}" class="btn btn-success bottom-action-button"><i class="fas fa-plus"></i> <span>Dodaj pracownika</span></a>@endif
    </header>
    {!! $grid !!}
@endsection
