@extends('layouts.app')

@section('content')
    <header>
        @if(\Request::route()->getName() == 'workers.getEdit')
            <h2>{{ __('Edycja pracownika') }}</h2>
        @else
            <h2>{{ __('Dodanie nowego pracownika') }}</h2>
        @endif
    </header>

    {!! Form::open('', 'POST') !!}
        {!! Form::addTabs([__('Szczegóły'), __('Historia')]) !!}

        {!! Form::addTab(__('Szczegóły')) !!}

        {!! Form::group(Form::input('text', 'first_name', __('Imię'), $user ? $user->first_name : '', true)) !!}
        {!! Form::group(Form::input('text', 'last_name', __('Nazwisko'), $user ? $user->last_name : '', true)) !!}
        {!! Form::group(Form::input('text', 'username', __('Login'), $user ? $user->username : '', true)) !!}
        {!! Form::group(Form::input('text', 'position', __('Stanowisko'), $user ? $user->position : '', true)) !!}
        {!! Form::group(Form::input('email', 'email', __('Adres e-mail'), $user ? $user->email : '', true)) !!}
        {!! Form::group(Form::input('password', 'password', __('Hasło'), '', true)) !!}
        {!! Form::checkbox('is_active', __('Użytkownik aktywny'), $user && $user->isActive(), false) !!}

        <h2>{{ __('Uprawnienia do modułów') }}</h2>
        <br>
        <div class="row">
        @foreach($privileges as $key => $privilege)
            <div class="col-3">
                <h5>{{ $privilege['name'] }}</h5>
                @foreach($privilege['abilities'] as $abKey => $ability)
                    {!! Form::checkbox('privileges['.$key.']['.$abKey.']', $ability, $user && $user->can($key.'.'.$abKey), false, true) !!}
                @endforeach
            </div>
        @endforeach
        </div>

        @if($user && \Auth::user()->isAdmin() && $user->id != \Auth::id())
            {!! Form::checkbox('is_admin', __('Administrator serwisu'), $user && $user->isAdmin(), false) !!}
        @endif

        {!! Form::addTab(__('Historia')) !!}

        {!! \App\Helpers\History::render($user) !!}

        {!! Form::closeTab() !!}

        {!! Form::group(Form::buttons([
            [
                'type' => 'submit',
                'class' => 'btn btn-success',
                'value' => __('Zapisz'),
            ],[
                'type' => 'link',
                'class' => 'btn btn-danger',
                'value' => __('Powrót'),
                'href' => route('workers.getIndex')
            ]
        ])) !!}
    {!! Form::close() !!}

@endsection
