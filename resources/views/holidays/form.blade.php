@extends('layouts.app')

@section('content')
    <header>
        <h1>{{ __('Dodanie nowego wniosku') }}</h1>
    </header>


    {!! Form::open('', 'POST') !!}
        {!! Form::addTabs([__('Szczegóły'), __('Historia')]) !!}

        {!! Form::addTab(__('Szczegóły')) !!}

        {!! Form::group(Form::datepicker('text', 'date_from', __('Data od'), $entity ? $entity->date_from : '', true)) !!}
        {!! Form::group(Form::datepicker('text', 'date_to', __('Data do'), $entity ? $entity->date_to : '', true)) !!}
        {!! Form::group(Form::input('textarea', 'description', __('Opis'), $entity ? $entity->description : '', true)) !!}
        {!! Form::group(Form::select2('status', __('Status'), $entity ? $entity->getStatuses() : [], $entity ? [$entity->status] : [1], '', false, false)) !!}
        {!! Form::group(Form::select2('type', __('Typ'), $entity ? $entity->getTypes() : [], $entity ? [$entity->status] : [1], '', false, false)) !!}

        {!! Form::addTab(__('Historia')) !!}

        {!! \App\Helpers\History::render($entity) !!}

        {!! Form::closeTab() !!}

        {!! Form::group(Form::buttons([
            [
                'type' => 'submit',
                'class' => 'btn btn-success',
                'value' => __('Zapisz'),
            ],[
                'type' => 'link',
                'class' => 'btn btn-danger',
                'value' => __('Powrót'),
                'href' => route('holidays.getIndex')
            ]
        ])) !!}
    {!! Form::close() !!}

@endsection
