@extends('layouts.app')

@section('content')
    <header>
        <h2>{{ __('Lista wniosków urlopowych') }}</h2>
        @if(\Auth::user()->checkPrivileges('holidays.Create'))<a href="{{ route('holidays.getCreate') }}" class="btn btn-success bottom-action-button"><i class="fas fa-plus"></i> <span>Dodaj wniosek</span></a>@endif
    </header>
    {!! $grid !!}
@endsection
