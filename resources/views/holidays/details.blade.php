@extends('layouts.app')

@section('content')
    <header>
        <h2>{{ __('Szczegóły wniosku') }}</h2>
    </header>
    <br>
    <br>
    {!! Form::open('', 'POST') !!}
    <form method="post">
        {!! Form::group(Form::row(__('Data od'), date('Y-m-d', strtotime($entity->date_from)))) !!}
        {!! Form::group(Form::row(__('Data do'), date('Y-m-d', strtotime($entity->date_to)))) !!}
        {!! Form::group(Form::row(__('Opis'), $entity->description)) !!}
        {!! Form::group(Form::select2('status', __('Status'), $entity ? $entity->getStatuses() : [], $entity ? [$entity->status] : [1], '', false, false)) !!}
        {!! Form::group(Form::row(__('Typ'), $entity->getType($entity->type))) !!}
        <br>
        <br>
        @php
            $buttons = [
                [
                    'type' => 'link',
                    'class' => 'btn btn-success',
                    'value' => __('Powrót'),
                    'href' => route('home')
                ]
            ];

            if(\Auth::user()->isAdmin() && $entity->user_id != \Auth::id() && $entity->status > 1)
            {
                $buttons[] = [
                    'type' => 'submit',
                    'class' => 'btn btn-success',
                    'value' => __('Zapisz'),
                ];
            }
        @endphp
        {!! Form::group(Form::buttons($buttons)) !!}
    {!! Form::close() !!}

@endsection
