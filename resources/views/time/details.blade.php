@extends('layouts.app')

@section('content')
    <header>
        <h1>{{ __('Szczegóły wykonanej czynności') }}</h1>
    </header>
    <br>
    <br>
    {!! Form::open('', 'POST') !!}
    <form method="post">
        {!! Form::group(Form::row(__('Projekt'), $entity->project->name)) !!}
        {!! Form::group(Form::row(__('Zadanie'), $entity->task->name)) !!}
        {!! Form::group(Form::row(__('Opis'), $entity->description)) !!}
        {!! Form::group(Form::row(__('Data'), $entity->date)) !!}
        {!! Form::group(Form::row(__('Ilość godzin'), $entity->hours)) !!}

        <br>
        <br>
        {!! Form::group(Form::buttons([
            [
                'type' => 'link',
                'class' => 'btn btn-success',
                'value' => __('Powrót'),
                'href' => route('time.getIndex')
            ]
        ])) !!}
    {!! Form::close() !!}

@endsection
