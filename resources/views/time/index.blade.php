@extends('layouts.app')

@section('content')
    <header>
        <h2>{{ __('Ewidencja czasu pracy') }}</h2>
        @if(\Auth::user()->checkPrivileges('time.Create'))<a href="{{ route('time.getCreate') }}" class="btn btn-success bottom-action-button"><i class="fas fa-plus"></i> <span>Dodaj czas pracy</span></a>@endif
        {!! Form::open('', 'post') !!}
        <br>
        <div class="col pl-0 pr-0">
            <div class="col-lg-1 col-md-2 col-sm-4 col-sm-6 float-right pl-0 pr-0">
                @if(isset($filters['date']) && !empty($filters['date']))
                    <span class="btn btn-danger" id="clearFilters">{{ __('Czyść') }}</span>
                @else
                    <input type="submit" class="btn btn-success" value="{{ __('Filtruj') }}" />
                @endif
            </div>
            <div class="col-lg-1 col-md-2 col-sm-4 col-sm-6 float-right pl-0 pr-0">
                <input type="text" name="date" id="jq-date" class="form-control" data-provide="datepicker" placeholder="{{ __('Data') }}" value="{{ Arr::get($filters, 'date', '') }}" />
                <script>
                    $(document).ready(function(){
                        $("#jq-date").datepicker({
                            dateFormat: 'yy-mm-dd',
                            monthNames: ["Styczeń", "Luty", "Marzec", "Kwiecień", "Maj", "Czerwiec", "Lipiec", "Sierpień", "Wrzesień", "Październik", "Listopad", "Grudzień"],
                            dayNamesMin: [ "Nd", "Po", "Wt", "Śr", "Czw", "Pt", "So" ],
                            beforeShow: function() {
                                setTimeout(function(){
                                    $("#jq-date").css("z-index", 11);
                                }, 0);
                            }
                        });
                        $("#clearFilters").on('click', function(){
                            $('#jq-date').val('');
                            $('#jq-date').closest('form').submit();
                        });
                    })
                </script>
            </div>
        </div>
        <div class="clearfix"></div>
        {!! Form::close() !!}
    </header>
    {!! $grid !!}
@endsection
