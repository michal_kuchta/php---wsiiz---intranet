@extends('layouts.app')

@section('content')
    <header>
        <h1>{{ __('Dodanie czasu pracy') }}</h1>
    </header>


    {!! Form::open('', 'POST') !!}

        @php
            $customQuery = "
                placeholder: '".__('Wybierz zadanie')."'
            ";
        @endphp

        {!! Form::group(Form::select2('project_id', __('Projekt'), $projects, $entity && $entity->projekt ? [$entity->projekt->id] : [], '', true, false, $customQuery, __('Wybierz projekt'))) !!}

        @php
            $customQuery = "
                data: function(params){
                    var query = {
                        search: params.term,
                        project: $('#jq-project_id-select2').val()
                    }
                    console.log(query);
                    return query;
                }
            ";
        @endphp

        {!! Form::group(Form::select2('task_id', __('Zadanie'), [], $entity && $entity->task ? [$entity->task->id] : [], route('api.time.tasks'), true, false, $customQuery, __('Wybierz zadanie'))) !!}
        {!! Form::group(Form::textarea('description', __('Opis wykonanych czynności'), $entity ? $entity->description : '', true)) !!}
        {!! Form::group(Form::datepicker('text', 'date', __('Data'), $entity ? $entity->date : '', true)) !!}
        {!! Form::group(Form::input('number', 'hours', __('Ilość godzin'), $entity ? $entity->hours : '', true)) !!}

        {!! Form::group(Form::buttons([
            [
                'type' => 'submit',
                'class' => 'btn btn-success',
                'value' => __('Zapisz'),
            ],[
                'type' => 'link',
                'class' => 'btn btn-danger',
                'value' => __('Powrót'),
                'href' => route('time.getIndex')
            ]
        ])) !!}
    {!! Form::close() !!}

@endsection
