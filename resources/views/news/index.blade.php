@extends('layouts.app')

@section('content')
    <header>
        <h2>{{ __('Aktualności') }}</h2>
        @if(\Auth::user()->checkPrivileges('news.Create'))<a href="{{ route('news.getCreate') }}" class="btn btn-success bottom-action-button"><i class="fas fa-plus"></i> <span>Dodaj aktualność</span></a>@endif
    </header>
    {!! $grid !!}
@endsection
