@extends('layouts.app')

@section('content')
    <header>
        <h1>{{ __('Czy jesteś pewien, że chcesz usunąć tę aktualność?') }}</h1>
    </header>
    <br>
    <br>
    {!! Form::open('', 'POST') !!}
    <form method="post">
        {!! Form::group(Form::row(__('Tytuł'), $entity->name)) !!}
        {!! Form::group(Form::row(__('Data publikacji'), $entity->published_at)) !!}
        {!! Form::group(Form::row(__('Data zakończenia publikacji'), $entity->unpublished_at)) !!}
        {!! Form::group(Form::row(__('Opis'), $entity->content)) !!}
        {!! Form::group(Form::row(__('Autor'), $entity->author ? $entity->author->getName() : '')) !!}

        @if(!empty($entity->images))
            <div class="row">
            @foreach($entity->images as $id => $image)
                <div class="col-4">
                    <img src="{{ $image }}" />
                </div>
            @endforeach
            </div>
        @endif

        <br>
        <br>
        {!! Form::group(Form::buttons([
            [
                'type' => 'submit',
                'class' => 'btn btn-danger',
                'value' => __('Usuń'),
            ],[
                'type' => 'link',
                'class' => 'btn btn-success',
                'value' => __('Powrót'),
                'href' => route('tasks.getIndex')
            ]
        ])) !!}
    {!! Form::close() !!}

@endsection
