@extends('layouts.app')

@section('content')
    <header>
        @if(\Request::route()->getName() == 'news.getEdit')
            <h2>{{ __('Edycja aktualności') }}</h2>
        @else
            <h1>{{ __('Dodanie nowej aktualności') }}</h1>
        @endif
    </header>


    {!! Form::open('', 'POST') !!}
        {!! Form::addTabs([__('Aktualność'), __('Historia')]) !!}

        {!! Form::addTab(__('Aktualność')) !!}
        <input type="hidden" name="author_id" value="{{ $entity && !empty($entity->author_id) ? $entity->author_id : Auth::id() }}" />
        {!! Form::group(Form::input('text', 'name', __('Tytuł'), $entity ? $entity->name : '', true)) !!}
        {!! Form::group(Form::input('editor', 'content', __('Treść'), $entity ? $entity->content : '', true)) !!}
        {!! Form::group(Form::datepicker('text', 'published_at', __('Data rozpoczęcia'), $entity ? $entity->published_at : '')) !!}
        {!! Form::group(Form::datepicker('text', 'unpublished_at', __('Data zakończenia'), $entity ? $entity->unpublished_at : '')) !!}

        {!! Form::group(Form::uploader('images', __('Zdjęcia'), route('api.uploadImages'), $entity && is_array($entity->images) ? $entity->images : [])) !!}

        {!! Form::addTab(__('Historia')) !!}

        {!! \App\Helpers\History::render($entity) !!}

        {!! Form::closeTab() !!}

        {!! Form::group(Form::buttons([
            [
                'type' => 'submit',
                'class' => 'btn btn-success',
                'value' => __('Zapisz'),
            ],[
                'type' => 'link',
                'class' => 'btn btn-danger',
                'value' => __('Powrót'),
                'href' => route('news.getIndex')
            ]
        ])) !!}
    {!! Form::close() !!}

@endsection
