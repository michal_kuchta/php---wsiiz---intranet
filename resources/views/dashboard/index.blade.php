@extends('layouts.app')

@section('content')
    <header>
        <h2>{{ __('Witaj') }} {!! \Auth()->user()->first_name !!}</h2>
    </header>
    <div class="row">
        <div class="col-md custom-table">
            <div class="custom-table__header">{{ __('Ostatnio utworzone projekty') }}</div>
            <div class="custom-table__body">
                @if(count($lastProjects) > 0)
                    @foreach($lastProjects as $project)
                        <div class="custom-table__body__row">
                            <a href="{{ route('projects.getDetails', [$project->id]) }}">{!! $project->name !!}</a>
                        </div>
                    @endforeach
                @endif
            </div>
            <div class="custom-table__footer"></div>
        </div>
        <div class="col-md custom-table">
            <div class="custom-table__header">{{ __('Ostatnio dodani pracownicy') }}</div>
            <div class="custom-table__body">
                @if(count($lastWorkers) > 0)
                    @foreach($lastWorkers as $worker)
                        <div class="custom-table__body__row">
                            <a href="{{ route('projects.getDetails', [$worker->id]) }}">{!! $worker->getName() !!}</a>
                        </div>
                    @endforeach
                @endif
            </div>
            <div class="custom-table__footer"></div>
        </div>
    </div>
    <div class="row">
        <div class="col-md custom-table">
            <div class="custom-table__header">{{ __('Twoje projekty') }}</div>
            <div class="custom-table__body">
                @if(count($myProjects) > 0)
                    @foreach($myProjects as $project)
                        <div class="custom-table__body__row">
                            <a href="{{ route('projects.getDetails', [$project->project->id]) }}">{!! $project->project->name !!}</a>
                        </div>
                    @endforeach
                @endif
            </div>
            <div class="custom-table__footer"></div>
        </div>
        <div class="col-md custom-table">
            <div class="custom-table__header">{{ __('Twoje zadania') }}</div>
            <div class="custom-table__body">
                @if(count($myTasks) > 0)
                    @foreach($myTasks as $myTask)
                        <div class="custom-table__body__row">
                            <a href="{{ route('tasks.getEdit', [$myTask->id]) }}">{{ $myTask->name }}</a>
                        </div>
                    @endforeach
                @endif
            </div>
            <div class="custom-table__footer"></div>
        </div>
    </div>

@endsection
