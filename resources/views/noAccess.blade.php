@extends('layouts.app')

@section('content')
    <div class="alert-danger">
        {{ __('Przykro mi. Nie posiadasz uprawnień do tego modułu. Skontaktuj się z administratorem') }}<bR>
        <b>Email:</b> <a href="mailto:admin@admin.pl">admin@admin.pl</a>
    </div>
@endsection
