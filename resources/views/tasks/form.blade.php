@extends('layouts.app')

@section('content')
    <header>
        @if(\Request::route()->getName() == 'tasks.getEdit')
            <h2>{{ __('Edycja zadania') }}</h2>
        @else
            <h1>{{ __('Dodanie nowego zadania') }}</h1>
        @endif
    </header>


    {!! Form::open('', 'POST') !!}

        {!! Form::addTabs([__('Szczegóły'), __('Historia')]) !!}

        {!! Form::addTab(__('Szczegóły')) !!}

        {!! Form::group(Form::input('text', 'name', __('Nazwa'), $entity ? $entity->name : '', true)) !!}
        {!! Form::group(Form::input('textarea', 'description', __('Opis zadania'), $entity ? $entity->description : '', true)) !!}
        {!! Form::group(Form::select2('project_id', __('Projekt'), $entity  ? $entity->getProject() : [], $entity  ? array_keys($entity->getProject()) : [], route('api.projects.getProjectsList'), true, false)) !!}
        {!! Form::group(Form::select2('signed_to', __('Przypisane do'), $entity  ? $entity->getSignedTo() : [], $entity  ? array_keys($entity->getSignedTo()) : [], route('api.workers.getUsersList'), false, false)) !!}
        {!! Form::group(Form::select2('status', __('Status'), $entity ? $entity->getStatuses() : [], $entity ? [$entity->status] : [1], '', false, false)) !!}
        {!! Form::group(Form::datepicker('text', 'date_start', __('Data rozpoczęcia'), $entity ? $entity->date_start : '', true)) !!}
        {!! Form::group(Form::datepicker('text', 'date_end', __('Data zakończenia'), $entity ? $entity->date_end : '', true)) !!}
        {!! Form::group(Form::input('number', 'hours', __('Ilość godzin'), $entity ? $entity->hours : '', true)) !!}
        {!! Form::checkbox('is_private', __('Zadanie prywatne'), $entity && $entity->isPrivate(), false) !!}

        {!! Form::addTab(__('Historia')) !!}

        {!! \App\Helpers\History::render($entity) !!}

        {!! Form::closeTab() !!}

        {!! Form::group(Form::buttons([
            [
                'type' => 'submit',
                'class' => 'btn btn-success',
                'value' => __('Zapisz'),
            ],[
                'type' => 'link',
                'class' => 'btn btn-danger',
                'value' => __('Powrót'),
                'href' => route('tasks.getIndex')
            ]
        ])) !!}
    {!! Form::close() !!}

@endsection
