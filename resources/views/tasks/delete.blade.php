@extends('layouts.app')

@section('content')
    <header>
        <h1>{{ __('Czy jesteś pewien, że chcesz usunąć to zadanie?') }}</h1>
    </header>
    <br>
    <br>
    {!! Form::open('', 'POST') !!}
    <form method="post">
        {!! Form::group(Form::row(__('Nazwa'), $entity->name)) !!}
        {!! Form::group(Form::row(__('Opis'), $entity->description)) !!}
        {!! Form::group(Form::row(__('Status'), Arr::get($entity->getStatuses(), $entity->status, ''))) !!}
        {!! Form::group(Form::row(__('Projekt'), $entity->project->project->name)) !!}
        {!! Form::group(Form::row(__('Przypisane do'), $entity->signedTo->getName())) !!}
        {!! Form::group(Form::row(__('Data rozpoczęcia'), $entity->date_start)) !!}
        {!! Form::group(Form::row(__('Data zakończenia'), $entity->date_end)) !!}

        <br>
        <br>
        {!! Form::group(Form::buttons([
            [
                'type' => 'submit',
                'class' => 'btn btn-danger',
                'value' => __('Usuń'),
            ],[
                'type' => 'link',
                'class' => 'btn btn-success',
                'value' => __('Powrót'),
                'href' => route('tasks.getIndex')
            ]
        ])) !!}
    {!! Form::close() !!}

@endsection
