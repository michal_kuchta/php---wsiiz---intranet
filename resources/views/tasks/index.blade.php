@extends('layouts.app')

@section('content')
    <header>
        <h2>{{ __('Lista zadań') }}</h2>
        @if(\Auth::user()->checkPrivileges('tasks.Create'))<a href="{{ route('tasks.getCreate') }}" class="btn btn-success bottom-action-button"><i class="fas fa-plus"></i> <span>Dodaj zadanie</span></a>@endif

        <div class="text-right">
            <form method="post" action="{{ route('tasks.postIndex') }}">
                @csrf
                {{ __('Wybierz projekt') }}:  <select name="selectProject" id="jq-selectProject">
                    <option value="__DELETE__DEFAULT__OPTION__">{{ __('-- Wybierz --') }}</option>
                    @foreach($projects as $key => $project)
                        <option value="{{ $key }}" {{ (Arr::get($filters, 'selectProject', 0) == $key ? 'selected="selected"':'') }}>{{ $project }}</option>
                    @endforeach
                </select>
            </form>
            <script>
                $(document).ready(function(){
                    $('#jq-selectProject').on('change', function(){
                        $(this)[0].form.submit();
                    });
                });
            </script>
        </div>
    </header>
    {!! $grid !!}
@endsection
