<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        @include('partials/head')
    </head>
    <body>
    <div class="loginPage">
        <section class="login">
            <div class="logo">
                <img src="/images/logo-white.svg" />
            </div>
            <header>
                <h1>{{ __('Zaloguj się') }}</h1>
            </header>
            <div class="loginForm">
                <form action="{{ route('login') }}" method="POST">
                    @csrf
                    <input type="text" name="username" placeholder="{{ __('Login') }}" class="form-control" />
                    <input type="password" id="password" name="password" placeholder="{{ __('Hasło') }}" class="form-control" />
                    @if ($errors->has('username') || $errors->has('password'))
                        <div align="center">
                            <p style="font-size: 20; color: #FF1C19;">{{ __('Nieprawidłowy login lub hasło') }}</p>
                        </div>
                    @endif
                    <button type="submit" class="btn btn-primary" >{{ __('Zaloguj') }}</button>
                </form>
            </div>
        </section>
    </div>
    <script>
        $('document').ready(function(){
            $('input[name="username"]').focus();
        });
    </script>
    </body>
</html>
