<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes(['register' => false, 'reset' => false, 'verify' => false]);

Route::middleware(['auth'])->group(function () {
    Route::get('/dashboard', 'DashboardController@getIndex')->name('home');
    Route::get('/', 'DashboardController@getIndex');

    /**
     * Pracownicy
     */
    Route::group(['prefix' => '/workers', 'middleware'=>['admin']], function(){
        Route::get('/{page?}', 'WorkersController@getIndex')->name('workers.getIndex')->where('page', '[0-9]+');
        Route::get('/details/{id?}', 'WorkersController@getDetails')->name('workers.getDetails');

        Route::get('/create', 'WorkersController@getCreate')->name('workers.getCreate');
        Route::post('/create', 'WorkersController@postCreate')->name('workers.postCreate');

        Route::get('/edit/{id?}', 'WorkersController@getEdit')->name('workers.getEdit');
        Route::post('/edit/{id?}', 'WorkersController@postEdit')->name('workers.postEdit');

        Route::get('/delete/{id?}', 'WorkersController@getDelete')->name('workers.getDelete');
        Route::post('/delete/{id?}', 'WorkersController@postDelete')->name('workers.postDelete');
    });

    /**
     * Projekty
     */
    Route::group(['prefix' => '/projects'], function(){
        Route::get('/{page?}', 'ProjectsController@getIndex')->name('projects.getIndex')->where('page', '[0-9]+');
        Route::get('/details/{id?}', 'ProjectsController@getDetails')->name('projects.getDetails');

        Route::get('/create', 'ProjectsController@getCreate')->name('projects.getCreate');
        Route::post('/create', 'ProjectsController@postCreate')->name('projects.postCreate');

        Route::get('/edit/{id?}', 'ProjectsController@getEdit')->name('projects.getEdit');
        Route::post('/edit/{id?}', 'ProjectsController@postEdit')->name('projects.postEdit');

        Route::get('/delete/{id?}', 'ProjectsController@getDelete')->name('projects.getDelete');
        Route::post('/delete/{id?}', 'ProjectsController@postDelete')->name('projects.postDelete');

        Route::get('/commits/{id?}/{page?}', 'ProjectsController@getCommits')->name('projects.getCommits')->where('page', '[0-9]+');
        Route::get('/gantt/{id?}', 'ProjectsController@getGanttChart')->name('projects.getGanttChart');

    });

    /**
     * Zadania
     */
    Route::group(['prefix' => '/tasks'], function(){
        Route::get('/{page?}', 'TasksController@getIndex')->name('tasks.getIndex')->where('page', '[0-9]+');
        Route::post('/', 'TasksController@postIndex')->name('tasks.postIndex');

        Route::get('/create', 'TasksController@getCreate')->name('tasks.getCreate');
        Route::post('/create', 'TasksController@postCreate')->name('tasks.postCreate');

        Route::get('/edit/{id?}', 'TasksController@getEdit')->name('tasks.getEdit');
        Route::post('/edit/{id?}', 'TasksController@postEdit')->name('tasks.postEdit');

        Route::get('/delete/{id?}', 'TasksController@getDelete')->name('tasks.getDelete');
        Route::post('/delete/{id?}', 'TasksController@postDelete')->name('tasks.postDelete');
    });

    /**
     * Urlopy
     */
    Route::group(['prefix' => '/holidays'], function(){
        Route::get('/{page?}', 'HolidaysController@getIndex')->name('holidays.getIndex')->where('page', '[0-9]+');

        Route::get('/create', 'HolidaysController@getCreate')->name('holidays.getCreate');
        Route::post('/create', 'HolidaysController@postCreate')->name('holidays.postCreate');

        Route::get('/details/{id?}', 'HolidaysController@getDetails')->name('holidays.getDetails');
    });

    /**
     * Zadania
     */
    Route::group(['prefix' => '/news'], function(){
        Route::get('/{page?}', 'NewsController@getIndex')->name('news.getIndex')->where('page', '[0-9]+');
        Route::post('/', 'NewsController@postIndex')->name('news.postIndex');

        Route::get('/create', 'NewsController@getCreate')->name('news.getCreate');
        Route::post('/create', 'NewsController@postCreate')->name('news.postCreate');

        Route::get('/edit/{id?}', 'NewsController@getEdit')->name('news.getEdit');
        Route::post('/edit/{id?}', 'NewsController@postEdit')->name('news.postEdit');

        Route::get('/delete/{id?}', 'NewsController@getDelete')->name('news.getDelete');
        Route::post('/delete/{id?}', 'NewsController@postDelete')->name('news.postDelete');
    });

    /**
     * Czas pracy
     */
    Route::group(['prefix' => '/time'], function(){
        Route::get('/{page?}', 'TimeController@getIndex')->name('time.getIndex')->where('page', '[0-9]+');
        Route::post('/', 'TimeController@postIndex')->name('time.postIndex');

        Route::get('/create', 'TimeController@getCreate')->name('time.getCreate');
        Route::post('/create', 'TimeController@postCreate')->name('time.postCreate');

        Route::get('/details/{id?}', 'TimeController@getDetails')->name('time.getDetails');
    });

    /**
     * Api
     */
    Route::group(['prefix' => '/api'], function(){
        Route::post('/upload/image', 'UploadController@save')->name('api.uploadImages');

        /**
         * Pracownicy
         */
        Route::group(['prefix' => '/workers'], function(){
            Route::get('/usersList', 'WorkersController@getUsersList')->name('api.workers.getUsersList');
        });

        /**
         * Projekty
         */
        Route::group(['prefix' => '/projects'], function(){
            Route::get('/projectsList', 'ProjectsController@getProjectsList')->name('api.projects.getProjectsList');
            Route::get('/ganttData/{id}', 'ProjectsController@getGanntData')->name('api.projects.getGanntData');
        });

        /**
         * Czas pracy
         */
        Route::group(['prefix' => '/time'], function(){
            Route::get('/tasks', 'TimeController@getTasks')->name('api.time.tasks');
        });
    });
});
